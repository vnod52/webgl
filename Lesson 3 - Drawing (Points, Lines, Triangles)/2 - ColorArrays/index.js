const CANVAS = document.getElementById("render-canvas");
const GL = CANVAS.getContext("webgl2");

var shader = {
    program: null,
    propertyLocationPosition: 0,
    propertyLocationPointSize: 0,
    propertyLocationColor: 0,

    vertexSource:
    `#version 300 es

    in vec4 position;
    in vec4 color;
    in float pointSize;

    out vec4 linkedColor;

    void main(){
        gl_Position = position;
        gl_PointSize = pointSize;
        linkedColor = color;
    }`,

    fragmentSource:
    `#version 300 es
    precision mediump float;

    in vec4 linkedColor;

    out vec4 outColor;

    void main(){
        outColor = linkedColor;
    }`

};

var positionBuffer;
var colorBuffer;

start();

function start(){
    //Create the size of the GL canvas
    GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);

    //Change the background color of the canvas
    GL.clearColor(0, 0, 0, 1);

    createVertices();

    shader.program = createShader();
    getPropertyLocation();
    setShaderProperties();
    
    draw();
    
}

function draw(){
    GL.clear(GL.COLOR_BUFFER_BIT);
    GL.drawArrays(GL.TRIANGLES, 0, 3);
    // GL.drawArrays(GL.TRIANGLES, 5, 10);
}

function createShader() {
    var vertexShader = getAndCompileShader(shader.vertexSource, GL.VERTEX_SHADER);
    var fragmentShader = getAndCompileShader(shader.fragmentSource,GL.FRAGMENT_SHADER);

    var newShaderProgram = GL.createProgram();
    GL.attachShader(newShaderProgram, vertexShader);
    GL.attachShader(newShaderProgram, fragmentShader);
    GL.linkProgram(newShaderProgram);
    GL.useProgram(shader.program);

    return newShaderProgram
}

function getAndCompileShader(shaderSource, shaderType) {
    var newShader = GL.createShader(shaderType);
    GL.shaderSource(newShader, shaderSource);
    GL.compileShader(newShader);

    console.log(shaderSource);

    if (!GL.getShaderParameter(newShader, GL.COMPILE_STATUS)){
        alert(GL.getShaderInfoLog(newShader));
        return null;
    }

    return newShader
}

function getPropertyLocation(){
    shader.propertyLocationPosition = GL.getAttribLocation(shader.program, "position");
    shader.propertyLocationPointSize = GL.getAttribLocation(shader.program, "pointSize");
    shader.propertyLocationColor = GL.getAttribLocation(shader.program, "color");

    GL.enableVertexAttribArray(shader.propertyLocationPosition);
    GL.enableVertexAttribArray(shader.propertyLocationColor);

}

function setShaderProperties() {
    GL.useProgram(shader.program);

    GL.bindBuffer(GL.ARRAY_BUFFER, positionBuffer);
    GL.vertexAttribPointer(shader.propertyLocationPosition, 3, GL.FLOAT, false, 0, 0);

    GL.bindBuffer(GL.ARRAY_BUFFER, colorBuffer);
    GL.vertexAttribPointer(shader.propertyLocationColor, 4, GL.FLOAT, false, 0, 0);

    GL.vertexAttrib1f(shader.propertyLocationPointSize, 20);
}

function createVertices() {
    var positions = [
        -0.5, -0.5, 0.0,
        0.5, -0.5, 0.0,
        -0.0, 0.5, 0.0,
    ];

    var colors = [
        1.0, 0.0, 0.5, 1.0,
        0.0, 1.0, 0.0, 1.0,
        0.6, 0.5, 1.0, 1.0,
    ];

    positionBuffer = GL.createBuffer();
    GL.bindBuffer(GL.ARRAY_BUFFER, positionBuffer);
    GL.bufferData(GL.ARRAY_BUFFER, new Float32Array(positions), GL.STATIC_DRAW);

    colorBuffer = GL.createBuffer();
    GL.bindBuffer(GL.ARRAY_BUFFER, colorBuffer);
    GL.bufferData(GL.ARRAY_BUFFER, new Float32Array(colors), GL.STATIC_DRAW);
}