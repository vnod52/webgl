const CANVAS = document.getElementById("render-canvas");
const GL = CANVAS.getContext("webgl2");
const FLOAT_BYTE_SIZE = 4;
const STRIDE = 6 * FLOAT_BYTE_SIZE;
const VERTEX_COLOR_OFFSET = 2 * FLOAT_BYTE_SIZE;
const VERTEX_COUNT = 5000;

var shader = {
    program: null,
    propertyLocationPosition: 0,
    propertyLocationPointSize: 0,
    propertyLocationColor: 0,

    vertexSource:
        `#version 300 es

    in vec4 position;
    in vec4 color;
    in float pointSize;

    out vec4 linkedColor;

    void main(){
        gl_Position = position;
        gl_PointSize = pointSize;
        linkedColor = color;
    }`,

    fragmentSource:
        `#version 300 es
    precision mediump float;

    in vec4 linkedColor;

    out vec4 outColor;

    void main(){
        outColor = linkedColor;
    }`

};

var vertexDataBuffer;
var vertices = [];
var mousePosition = {
    x: 0,
    y: 0
};

CANVAS.addEventListener("mousemove", (e) => {
    mousePosition.x = remapValue(e.clientX, 0, CANVAS.clientWidth, -1, 1);
    mousePosition.y = remapValue(e.clientY, 0, CANVAS.clientWidth, 1, -1);

    console.log("x:", mousePosition.x.toFixed(2), "y:", mousePosition.y.toFixed(2));
});

start();

function start() {
    //Create the size of the GL canvas
    GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);

    //Change the background color of the canvas
    GL.clearColor(0, 0, 0, 1);

    createVertices();

    shader.program = createShader();
    getPropertyLocation();
    setShaderProperties();

    requestAnimationFrame(draw);

}

function draw() {
    GL.clear(GL.COLOR_BUFFER_BIT);

    moveVertices();

    GL.bufferSubData(GL.ARRAY_BUFFER, 0, new Float32Array(vertices));

    GL.drawArrays(GL.POINTS, 0, VERTEX_COUNT);

    requestAnimationFrame(draw);
}

function createShader() {
    var vertexShader = getAndCompileShader(shader.vertexSource, GL.VERTEX_SHADER);
    var fragmentShader = getAndCompileShader(shader.fragmentSource, GL.FRAGMENT_SHADER);

    var newShaderProgram = GL.createProgram();
    GL.attachShader(newShaderProgram, vertexShader);
    GL.attachShader(newShaderProgram, fragmentShader);
    GL.linkProgram(newShaderProgram);
    GL.useProgram(shader.program);

    return newShaderProgram
}

function getAndCompileShader(shaderSource, shaderType) {
    var newShader = GL.createShader(shaderType);
    GL.shaderSource(newShader, shaderSource);
    GL.compileShader(newShader);

    console.log(shaderSource);

    if (!GL.getShaderParameter(newShader, GL.COMPILE_STATUS)) {
        alert(GL.getShaderInfoLog(newShader));
        return null;
    }

    return newShader
}

function getPropertyLocation() {
    shader.propertyLocationPosition = GL.getAttribLocation(shader.program, "position");
    shader.propertyLocationPointSize = GL.getAttribLocation(shader.program, "pointSize");
    shader.propertyLocationColor = GL.getAttribLocation(shader.program, "color");

    GL.enableVertexAttribArray(shader.propertyLocationPosition);
    GL.enableVertexAttribArray(shader.propertyLocationColor);

}

function setShaderProperties() {
    GL.useProgram(shader.program);

    GL.vertexAttribPointer(shader.propertyLocationPosition, 2, GL.FLOAT, false, STRIDE, 0);
    GL.vertexAttribPointer(shader.propertyLocationColor, 4, GL.FLOAT, false, STRIDE, VERTEX_COLOR_OFFSET);

    GL.vertexAttrib1f(shader.propertyLocationPointSize, 1.5);
}

function createVertices() {
    for (let i = 0; i < VERTEX_COUNT; i++) {
        vertices.push(Math.random() * 2 - 1);
        vertices.push(Math.random() * 2 - 1);

        vertices.push(Math.random());
        vertices.push(Math.random());
        vertices.push(Math.random());
        vertices.push(1);
    }

    vertexDataBuffer = GL.createBuffer();
    GL.bindBuffer(GL.ARRAY_BUFFER, vertexDataBuffer);
    GL.bufferData(GL.ARRAY_BUFFER, new Float32Array(vertices), GL.DYNAMIC_DRAW);

}

function moveVertices() {

    for (let i = 0; i < vertices.length; i += STRIDE / FLOAT_BYTE_SIZE) {
        var difference = {
            x: vertices[i] - mousePosition.x,
            y: vertices[i + 1] - mousePosition.y,
        }




        var distanceFromMouse = Math.sqrt(
            Math.pow(difference.x, 2) + Math.pow(difference.y, 2)
        )

        var inRangeDistance = 0.2;
        var pushDistance = inRangeDistance * 1.1;

        if (distanceFromMouse < inRangeDistance && mousePosition.x != 0) {
            vertices[i] = mousePosition.x + difference.x / distanceFromMouse * pushDistance;
            vertices[i + 1] = mousePosition.y + difference.y / distanceFromMouse * pushDistance;

            vertices[i + 3] = 1;

        } else {
            vertices[i] += Math.random() * 0.01 - 0.005;
            vertices[i + 1] += Math.random() * 0.01 - 0.005;

        }
    }
}

function remapValue(value, minSrc, maxSrc, minDst, maxDst) {
    return (value - minSrc) / (maxSrc - minSrc) * (maxDst - minDst) + minDst;
}