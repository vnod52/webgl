class Camera {
    constructor() {
        this.update();
    }

    projectionMatrix = mat4.create();
    viewMatrix = mat4.create();
    near = 1;
    far = 200;
    fovRadians = 60 * Math.PI/180;
    //We are working here
    moveSpeed = 0.2;
    rotateSpeed = 0.005;
    pitch = 0;
    yaw = -1 * Math.PI/2;
    target = vec3.create();
    position = vec3.create();
    lookDirection = vec3.fromValues(0, 0, -1);

    update() {
        if (CANVAS.width != CANVAS.clientWidth || CANVAS.height != CANVAS.clientHeight){
            this.updateViewport();
        }
        this.updateProjectionMatrix();
        this.updateViewMatrix();

    }

    updateViewport() {
        CANVAS.width = CANVAS.clientWidth;
        CANVAS.height = CANVAS.clientHeight;
        GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);
    }

    updateProjectionMatrix() {
        var aspect = CANVAS.clientWidth / CANVAS.clientHeight;
        mat4.perspective(this.projectionMatrix, this.fovRadians, aspect, this.near, this.far);

        GL.uniformMatrix4fv(WEBGL_APP.shader.propertyLocationProjectionMatrix, false, this.projectionMatrix);
    }

    updateViewMatrix() {
        this.moveCamera();
        this.rotateCamera();

        vec3.add(this.target, this.position, this.lookDirection)
        mat4.lookAt(this.viewMatrix, this.position, this.target, vec3.fromValues(0, 1, 0));

        GL.uniformMatrix4fv(WEBGL_APP.shader.propertyLocationViewMatrix, false, this.viewMatrix);
    }

    moveCamera() {
        var input = WEBGL_APP.input;

        if(input.pressed.w) {
            this.position[2] -= this.moveSpeed;
        }
        if(input.pressed.s) {
            this.position[2] += this.moveSpeed;
        }
        if(input.pressed.a) {
            this.position[0] += this.moveSpeed;
        }
        if(input.pressed.d) {
            this.position[0] -= this.moveSpeed;
        }


    }

    rotateCamera() {
        if (!WEBGL_APP.isLocked) {
            return; 
        }
        
        var mouseDelta = WEBGL_APP.input.mouseDelta;

        this.yaw += mouseDelta[0] * this.rotateSpeed;
        this.pitch -= mouseDelta[1] * this.rotateSpeed;
        this.pitch = WEBGL_APP.mathUtils.clamp(this.pitch, -1.5, 1.5);

        this.lookDirection[0] = Math.cos(this.pitch) * Math.cos(this.yaw);
        this.lookDirection[1] = Math.sin(this.pitch);
        this.lookDirection[2] = Math.cos(this.pitch) * Math.sin(this.yaw);

    }
}