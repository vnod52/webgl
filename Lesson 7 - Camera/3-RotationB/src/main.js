const { mat4, vec2, vec3 } = glMatrix;

const CANVAS = document.getElementById("render-canvas");
const GL = CANVAS.getContext("webgl2");
const WEBGL_APP = new WebGLApp();

WEBGL_APP.initialize();