class Input {
    constructor() {
        this.setKeyboardListener();

    }

    pressed = {
        w: false,
        a: false,
        s: false,
        d: false     
    }

    setKeyboardListener() {
        document.addEventListener("keydown",(e) => {
            console.log(e.key + " - KeyCode:" + e.keyCode);
            if (e.key == 'd' || e.keyCode.toString() == 39) {
                this.pressed.d = true;
            } 
            if (e.key == 'w') {
                this.pressed.w= true;
            }
            if (e.key == 'a') {
                this.pressed.a = true;
            }
            if (e.key == 's') {
                this.pressed.s = true;
            }
                

        }); 

        document.addEventListener("keyup",(e) => {
            //console.log(e.key);
            if (e.key == 'd') {
                this.pressed.d = false;
            } 
            if (e.key == 'w') {
                this.pressed.w = false;
            }
            if (e.key == 'a') {
                this.pressed.a = false;
            }
            if (e.key == 's') {
                this.pressed.s = false;
            }
                

        }); 
    }
}