
const CANVAS = document.getElementById("render-canvas");
const GL = CANVAS.getContext("webgl2");
const WEBGL_APP = new WebGLApp();
const { mat4, vec3 } = glMatrix;

WEBGL_APP.initialize();