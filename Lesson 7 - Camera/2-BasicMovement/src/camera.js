class Camera {
    constructor() {
        this.update();
    }

    projectionMatrix = mat4.create();
    viewMatrix = mat4.create();
    near = 1;
    far = 200;
    fovRadians = 60 * Math.PI/180;
    target = vec3.create();
    position = vec3.create();
    moveSpeed = 0.2;

    update() {
        if (CANVAS.width != CANVAS.clientWidth || CANVAS.height != CANVAS.clientHeight){
            this.updateViewport();
        }
        this.updateProjectionMatrix();
        this.updateViewMatrix();

    }

    updateViewport() {
        CANVAS.width = CANVAS.clientWidth;
        CANVAS.height = CANVAS.clientHeight;
        GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);
    }

    updateProjectionMatrix() {
        var aspect = CANVAS.clientWidth / CANVAS.clientHeight;
        mat4.perspective(this.projectionMatrix, this.fovRadians, aspect, this.near, this.far);

        GL.uniformMatrix4fv(WEBGL_APP.shader.propertyLocationProjectionMatrix, false, this.projectionMatrix);
    }

    updateViewMatrix() {
        this.moveCamera();

        vec3.add(this.target, this.position, vec3.fromValues(0, 0, -1))
        mat4.lookAt(this.viewMatrix, this.position, this.target, vec3.fromValues(0, 1, 0));

        GL.uniformMatrix4fv(WEBGL_APP.shader.propertyLocationViewMatrix, false, this.viewMatrix);
    }

    moveCamera() {
        var input = WEBGL_APP.input;

        if(input.pressed.w) {
            this.position[2] -= this.moveSpeed;
        }
        if(input.pressed.s) {
            this.position[2] += this.moveSpeed;
        }
        if(input.pressed.a) {
            this.position[0] += this.moveSpeed;
        }
        if(input.pressed.d) {
            this.position[0] -= this.moveSpeed;
        }


    }
}