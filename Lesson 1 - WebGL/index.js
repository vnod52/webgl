const CANVAS = document.getElementById("render-canvas");
const GL = CANVAS.getContext("webgl2");

start();

function start(){
    //Create the size of the GL canvas
    GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);

    //Change the background color of the canvas
    GL.clearColor(0, 0, 0, 1);

    draw();
    
}

function draw(){
    GL.clear(GL.COLOR_BUFFER_BIT);
}