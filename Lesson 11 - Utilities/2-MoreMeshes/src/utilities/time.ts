export class Time {
    constructor(isDebuy = false) {
        Time.IS_DEBUG = isDebuy;
    }

    static IS_DEBUG: boolean = false;
    static ELAPSED_TIME: number = 0;
    static DELTA_TIME: number = 0;

    startTime: Date = new Date();
    previousElapsedTime: number = 0;

    update() {
        this.setTime();
        this.setDeltaTime();

        if (Time.IS_DEBUG) {
            this.debug();
        }
    }

    setTime() {
        Time.ELAPSED_TIME = (new Date().getTime() - this.startTime.getTime()) / 1000;
    }

    setDeltaTime() {
        Time.DELTA_TIME = Time.ELAPSED_TIME - this.previousElapsedTime;
        this.previousElapsedTime = Time.ELAPSED_TIME;
    }

    debug() {
        console.log(
            `timeElapsed: ${Time.ELAPSED_TIME}
            \ndeltaTime: ${Time.DELTA_TIME}`
        )
    }
}