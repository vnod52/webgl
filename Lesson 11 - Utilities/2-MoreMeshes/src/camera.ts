import { mat4, vec3 } from "gl-matrix";
import { MAIN } from "./main";
import { Shader } from "./shaders/shader";
import { ShaderObject } from "./shaders/shaderObject";

export class Camera {
    constructor() {
        this.update();
    }

    viewMatrix      : mat4 = mat4.create();
    projectionMatrix: mat4 = mat4.create();
    near            : number = 1;
    far             : number = 200;
    fovRadians      : number = 60 * Math.PI/180;
    moveSpeed       : number = 0.2;
    rotateSpeed     : number = 0.005;
    pitch           : number = 0;
    yaw             : number = -1 * Math.PI/2;
    target          : vec3 = vec3.create();
    position        : vec3 = vec3.create();
    lookDirection   : vec3 = vec3.fromValues(0, 0, -1);

    update() {
        if (MAIN.CANVAS.width != MAIN.CANVAS.clientWidth || MAIN.CANVAS.height != MAIN.CANVAS.clientHeight) {
            this.updateViewport();
        }

        this.updateProjectionMatrix();
        this.updateViewMatrix();

        this.updateShader3fv(MAIN.WEBGL_APP.shaderObject);
        this.updateShaderUniform(MAIN.WEBGL_APP.shaderObject);
        this.updateShaderUniform(MAIN.WEBGL_APP.shaderLightGizmo);
    }

    updateShaderUniform(_shader: Shader) {
        MAIN.GL.useProgram(_shader.program);
        MAIN.GL.uniformMatrix4fv(_shader.propertyLocationProjectionMatrix, false, this.projectionMatrix);
        MAIN.GL.uniformMatrix4fv(_shader.propertyLocationViewMatrix, false, this.viewMatrix);
    }

    updateShader3fv(_shader: Shader) {
        MAIN.GL.useProgram(_shader.program);
        MAIN.GL.uniform3fv((<ShaderObject>_shader).propertyLocationProjectionMatrix, this.position);
    }

    updateViewport() {
        MAIN.CANVAS.width = MAIN.CANVAS.clientWidth;
        MAIN.CANVAS.height = MAIN.CANVAS.clientHeight;
        MAIN.GL.viewport(0, 0, MAIN.CANVAS.clientWidth, MAIN.CANVAS.clientHeight);
    }

    updateProjectionMatrix() {
        var aspect: number = MAIN.CANVAS.clientWidth / MAIN.CANVAS.clientHeight;

        // var projectionMatrix = this.perspective(aspect);
        mat4.perspective(
            this.projectionMatrix
        ,   this.fovRadians
        ,   aspect
        ,   this.near
        ,   this.far
        );
    }

    updateViewMatrix() {
        this.moveCamera();
        this.rotateCamera();

        vec3.add(this.target, this.position, this.lookDirection);
        mat4.lookAt(this.viewMatrix, this.position, this.target, vec3.fromValues(0, 1, 0));
    }

    moveCamera() {
        var forward: any = vec3.create();
        vec3.normalize(forward, this.lookDirection);
        var right: any = vec3.create();
        vec3.cross(right, forward, vec3.fromValues(0, 1, 0));
        var up: any = vec3.create();
        vec3.cross(up, forward, right);

        var input: any = MAIN.WEBGL_APP.input;
        var movementDirection: any = vec3.create();

        if (input.pressed.w) {
            vec3.scale(movementDirection, forward, this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.s) {
            vec3.scale(movementDirection, forward, -this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.a) {
            vec3.scale(movementDirection, right, -this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.d) {
            vec3.scale(movementDirection, right, this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        // up/down
        if (input.pressed.q) {
            vec3.scale(movementDirection, up, this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.e) {
            vec3.scale(movementDirection, up, -this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
    }

    rotateCamera() {
        if (!MAIN.WEBGL_APP.isLocked) { return; }

        var mouseDelta: any = MAIN.WEBGL_APP.input.mouseDelta;

        this.yaw += mouseDelta[0] * this.rotateSpeed;
        this.pitch -= mouseDelta[1] * this.rotateSpeed;
        this.pitch = MAIN.WEBGL_APP.mathUtils.clamp(this.pitch, -1.5, 1.5);

        this.lookDirection[0] = Math.cos(this.pitch) * Math.cos(this.yaw);
        this.lookDirection[1] = Math.sin(this.pitch);
        this.lookDirection[2] = Math.cos(this.pitch) * Math.sin(this.yaw);
    }
}