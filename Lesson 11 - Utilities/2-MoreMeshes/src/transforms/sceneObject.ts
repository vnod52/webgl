import { Transform } from "./transform";
import { MAIN } from "../main";
import { vec3 } from "gl-matrix";
import { Time } from "../utilities/time";
import { Mesh } from "../mesh";

export class SceneObject extends Transform {
    constructor(_mesh: Mesh, _position: vec3 = vec3.fromValues(0,0,0), _rotation: vec3 = vec3.fromValues(0,0,0), _scale: vec3 = vec3.fromValues(1,1,1)) {
        super(MAIN.WEBGL_APP.shaderObject, _position, _rotation, _scale);

        this.mesh = _mesh;
        this.initialPosition = _position;
    }

    mesh: Mesh;
    angle: number       = Math.random() * (360 - -180) + -180;
    angleSpeed: number  = Math.random() * (3 - 1.5) + 1.5;
    offset: number      = 0;
    offsetSpeed: number = Math.random() * (2 - -1) + 1;
    initialPosition: vec3;

    update() {
        // this.transform();
        this.applyTransforms();

        this.draw();
    }

    draw() {
        MAIN.GL.useProgram(MAIN.WEBGL_APP.shaderObject.program);

        MAIN.GL.bindBuffer(MAIN.GL.ARRAY_BUFFER, this.mesh.vertexBuffer);
        this.mesh.setShaderProperties();
        MAIN.GL.drawArrays(MAIN.GL.TRIANGLES, 0, this.mesh.vertexCount);
    }

    transform() {
        this.offset += this.offsetSpeed * Time.DELTA_TIME;
        this.position = vec3.fromValues(
            this.initialPosition[0] + Math.sin(this.offset)
        ,   this.initialPosition[1] + Math.cos(this.offset)
        ,   -3
        );
        
        this.angle += (Math.PI * 2) * (0.5 / 360) * this.angleSpeed * Time.DELTA_TIME;
        this.rotation = vec3.fromValues(
            this.angle
        ,   this.angle * 2
        ,   0
        )

        this.scale = vec3.fromValues(
            Math.sin(Time.ELAPSED_TIME)
        ,   Math.cos(Time.ELAPSED_TIME)
        ,   1
        );
    }
}