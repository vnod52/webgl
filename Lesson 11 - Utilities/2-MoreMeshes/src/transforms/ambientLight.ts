import { vec3 } from "gl-matrix";
import { MAIN } from "../main";
import { Transform } from "./transform";
import { Time } from "../utilities/time";

export class AmbientLight extends Transform {
    constructor(_position: vec3 = vec3.fromValues(0,0,0), _rotation: vec3 = vec3.fromValues(0,0,0), _scale: vec3 = vec3.fromValues(.25, .25, .25)) {
        super(MAIN.WEBGL_APP.shaderLightGizmo, _position, _rotation, _scale);
        
        this.updateAmbientLight();
    }

    color: vec3 = vec3.fromValues(0.4, 0.4, 1);
    intensity: number = 1;
    speed: vec3 = vec3.fromValues(3, 2.2, 1.7);
    distance: vec3 = vec3.fromValues(1, 1.5, 1);

    updateAmbientLight() {
        // Use the object shader for sending light.
        MAIN.GL.useProgram(MAIN.WEBGL_APP.shaderObject.program);

        MAIN.GL.uniform3fv(MAIN.WEBGL_APP.shaderObject.propertyLocationLightPosition, this.position);
        MAIN.GL.uniform3fv(MAIN.WEBGL_APP.shaderObject.propertyLocationAmbientColor, this.color);
        MAIN.GL.uniform1f(MAIN.WEBGL_APP.shaderObject.propertyLocationAmbientIntensity, this.intensity);

        // Use the light gizmo shader for sending colour to our gizmo.
        MAIN.GL.useProgram(MAIN.WEBGL_APP.shaderLightGizmo.program);
        MAIN.GL.uniform3fv(MAIN.WEBGL_APP.shaderLightGizmo.propertyLocationColor, this.color);
        MAIN.GL.uniform1f(MAIN.WEBGL_APP.shaderLightGizmo.propertyLocationIntensity, this.intensity);
    }

    update() {
        this.transform();
        // this.position = vec3.fromValues(0, 2, -5);
        this.applyTransforms();

        MAIN.GL.useProgram(MAIN.WEBGL_APP.shaderObject.program);
        MAIN.GL.uniform3fv(MAIN.WEBGL_APP.shaderObject.propertyLocationLightPosition, this.position);

        this.draw();
    }

    private transform() {
        this.position = vec3.fromValues(
            Math.cos(Time.ELAPSED_TIME * this.speed[0]) *this.distance[0],
            Math.cos(Time.ELAPSED_TIME * this.speed[1]) *this.distance[1],
            Math.cos(Time.ELAPSED_TIME * this.speed[2]) *this.distance[2],
        );
        this.scale = vec3.fromValues(0.2, 0.2, 0.2);
    }

    draw() {
        MAIN.GL.useProgram(MAIN.WEBGL_APP.shaderLightGizmo.program);

        MAIN.GL.bindBuffer(MAIN.GL.ARRAY_BUFFER, MAIN.WEBGL_APP.cube.vertexBuffer);
        MAIN.GL.drawArrays(MAIN.GL.TRIANGLES, 0, MAIN.WEBGL_APP.cube.vertexCount);
    }
}