import { MAIN } from "./main";
import { ShaderObject } from "./shaders/shaderObject";

const FLOAT_BYTE_SIZE: number = 4;
const STRIDE: number = 12 * FLOAT_BYTE_SIZE;
const VERTEX_COLOR_OFFSET: number = 3 * FLOAT_BYTE_SIZE;
const VERTEX_TEX_COORD_OFFSET: number = 7 * FLOAT_BYTE_SIZE;
const VERTEX_NORMALS_OFFSET: number = 9 * FLOAT_BYTE_SIZE;

export class Mesh {
    constructor(_vertexArray: number[]) {
        this .vertexCount = _vertexArray.length / (STRIDE / FLOAT_BYTE_SIZE);
        console.log("vertexCount: ", this.vertexCount);

        this.createVertices(_vertexArray);
        this.setShaderProperties();
    }

    static PYRAMID: number[] = [
        -1.0, -0.5, -1.0,   1.0, 0.0, 0.0, 1.0,    0.0, 0.0,    0, 0, -1, //back
         1.0, -0.5, -1.0,   0.0, 1.0, 0.0, 1.0,    1.0, 0.0,    0, 0, -1,
         0.0,  1.0,  0.0,   0.0, 0.0, 1.0, 1.0,    0.5, 1.0,    0, 0, -1,

        -1.0, -0.5,  1.0,   0.0, 1.0, 0.0, 1.0,    0.0, 0.0,    0, 0,  1, //forward
         1.0, -0.5,  1.0,   1.0, 0.0, 0.0, 1.0,    1.0, 0.0,    0, 0,  1,
         0.0,  1.0,  0.0,   0.0, 0.0, 1.0, 1.0,    0.5, 1.0,    0, 0,  1,

        -1.0, -0.5, -1.0,   1.0, 0.0, 0.0, 1.0,    0.0, 0.0,    -1, 0, 0, //left
        -1.0, -0.5,  1.0,   0.0, 1.0, 0.0, 1.0,    1.0, 0.0,    -1, 0, 0,
         0.0,  1.0,  0.0,   0.0, 0.0, 1.0, 1.0,    0.5, 1.0,    -1, 0, 0,

         1.0, -0.5,  1.0,   1.0, 0.0, 0.0, 1.0,    0.0, 0.0,    1, 0, 0, //right
         1.0, -0.5, -1.0,   0.0, 1.0, 0.0, 1.0,    1.0, 0.0,    1, 0, 0,
         0.0,  1.0,  0.0,   0.0, 0.0, 1.0, 1.0,    0.5, 1.0,    1, 0, 0,
        
        -1.0, -0.5, -1.0,   1.0, 0.0, 0.0, 1.0,    0.0, 0.0,    0, -1, 0, //down
        -1.0, -0.5,  1.0,   0.0, 1.0, 0.0, 1.0,    0.0, 1.0,    0, -1, 0,
         1.0, -0.5,  1.0,   1.0, 0.0, 0.0, 1.0,    1.0, 1.0,    0, -1, 0,
        
        -1.0, -0.5, -1.0,   1.0, 0.0, 0.0, 1.0,    0.0, 0.0,    0, -1, 0, //down
         1.0, -0.5,  1.0,   1.0, 0.0, 0.0, 1.0,    1.0, 1.0,    0, -1, 0,
         1.0, -0.5, -1.0,   0.0, 1.0, 0.0, 1.0,    1.0, 0.0,    0, -1, 0,
    ];

    // Add a cube.
    static CUBE: number[] = [
        -0.5, -0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     0.0, 0.0,        0.0,  0.0, -1.0,
         0.5, -0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     1.0, 0.0,        0.0,  0.0, -1.0,
         0.5,  0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     1.0, 1.0,        0.0,  0.0, -1.0,
         0.5,  0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     1.0, 1.0,        0.0,  0.0, -1.0,
        -0.5,  0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     0.0, 1.0,        0.0,  0.0, -1.0,
        -0.5, -0.5, -0.5,       1.0, 0.0, 0.0, 1.0,     0.0, 0.0,        0.0,  0.0, -1.0,
        
        -0.5, -0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     0.0, 0.0,        0.0,  0.0,  1.0,
         0.5, -0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     1.0, 0.0,        0.0,  0.0,  1.0,
         0.5,  0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     1.0, 1.0,        0.0,  0.0,  1.0,
         0.5,  0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     1.0, 1.0,        0.0,  0.0,  1.0,
        -0.5,  0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     0.0, 1.0,        0.0,  0.0,  1.0,
        -0.5, -0.5,  0.5,       0.0, 1.0, 0.0, 1.0,     0.0, 0.0,        0.0,  0.0,  1.0,
        
        -0.5,  0.5,  0.5,       0.0, 0.0, 1.0, 1.0,     1.0, 0.0,       -1.0,  0.0,  0.0,
        -0.5,  0.5, -0.5,       0.0, 0.0, 1.0, 1.0,     1.0, 1.0,       -1.0,  0.0,  0.0,
        -0.5, -0.5, -0.5,       0.0, 0.0, 1.0, 1.0,     0.0, 1.0,       -1.0,  0.0,  0.0,
        -0.5, -0.5, -0.5,       0.0, 0.0, 1.0, 1.0,     0.0, 1.0,       -1.0,  0.0,  0.0,
        -0.5, -0.5,  0.5,       0.0, 0.0, 1.0, 1.0,     0.0, 0.0,       -1.0,  0.0,  0.0,
        -0.5,  0.5,  0.5,       0.0, 0.0, 1.0, 1.0,     1.0, 0.0,       -1.0,  0.0,  0.0,

         0.5,  0.5,  0.5,       1.0, 1.0, 0.0, 1.0,     0.0, 1.0,        1.0,  0.0,  0.0,
         0.5,  0.5, -0.5,       1.0, 1.0, 0.0, 1.0,     1.0, 1.0,        1.0,  0.0,  0.0,
         0.5, -0.5, -0.5,       1.0, 1.0, 0.0, 1.0,     1.0, 0.0,        1.0,  0.0,  0.0,
         0.5,  0.5,  0.5,       1.0, 1.0, 0.0, 1.0,     0.0, 1.0,        1.0,  0.0,  0.0,
         0.5, -0.5,  0.5,       1.0, 1.0, 0.0, 1.0,     0.0, 0.0,        1.0,  0.0,  0.0,
         0.5, -0.5, -0.5,       1.0, 1.0, 0.0, 1.0,     1.0, 0.0,        1.0,  0.0,  0.0,
        
        -0.5, -0.5, -0.5,       0.0, 1.0, 1.0, 1.0,     0.0, 1.0,        0.0, -1.0,  0.0,
         0.5, -0.5, -0.5,       0.0, 1.0, 1.0, 1.0,     1.0, 1.0,        0.0, -1.0,  0.0,
         0.5, -0.5,  0.5,       0.0, 1.0, 1.0, 1.0,     1.0, 0.0,        0.0, -1.0,  0.0,
         0.5, -0.5,  0.5,       0.0, 1.0, 1.0, 1.0,     1.0, 0.0,        0.0, -1.0,  0.0,
        -0.5, -0.5,  0.5,       0.0, 1.0, 1.0, 1.0,     0.0, 0.0,        0.0, -1.0,  0.0,
        -0.5, -0.5, -0.5,       0.0, 1.0, 1.0, 1.0,     0.0, 1.0,        0.0, -1.0,  0.0,
        
        -0.5,  0.5, -0.5,       1.0, 0.0, 1.0, 1.0,     0.0, 1.0,        0.0,  1.0,  0.0,
         0.5,  0.5, -0.5,       1.0, 0.0, 1.0, 1.0,     1.0, 1.0,        0.0,  1.0,  0.0,
         0.5,  0.5,  0.5,       1.0, 0.0, 1.0, 1.0,     1.0, 0.0,        0.0,  1.0,  0.0,
         0.5,  0.5,  0.5,       1.0, 0.0, 1.0, 1.0,     1.0, 0.0,        0.0,  1.0,  0.0,
        -0.5,  0.5,  0.5,       1.0, 0.0, 1.0, 1.0,     0.0, 0.0,        0.0,  1.0,  0.0,
        -0.5,  0.5, -0.5,       1.0, 0.0, 1.0, 1.0,     0.0, 1.0,        0.0,  1.0,  0.0
    ];

    vertexCount: number;
    vertexBuffer: WebGLBuffer;

    createVertices(_vertexArray: number[]) {    
        this.vertexBuffer = <WebGLBuffer>MAIN.GL.createBuffer();
        MAIN.GL.bindBuffer(MAIN.GL.ARRAY_BUFFER, this.vertexBuffer);
        MAIN.GL.bufferData(MAIN.GL.ARRAY_BUFFER, new Float32Array(_vertexArray), MAIN.GL.STATIC_DRAW);
    }

    setShaderProperties() {
        var shader: ShaderObject = MAIN.WEBGL_APP.shaderObject;
        MAIN.GL.useProgram(shader.program);

        MAIN.GL.vertexAttribPointer(shader.propertyLocationPosition, 3, MAIN.GL.FLOAT, false, STRIDE, 0);
        MAIN.GL.vertexAttribPointer(shader.propertyLocationColor, 4, MAIN.GL.FLOAT, false, STRIDE, VERTEX_COLOR_OFFSET);
        MAIN.GL.vertexAttribPointer(shader.propertyLocationTexCoord, 2, MAIN.GL.FLOAT, false, STRIDE, VERTEX_TEX_COORD_OFFSET);
        MAIN.GL.vertexAttribPointer(shader.propertyLocationVertexNormal, 3, MAIN.GL.FLOAT, false, STRIDE, VERTEX_NORMALS_OFFSET);

        MAIN.GL.vertexAttrib1f(shader.propertyLocationPointSize, 50);
    }
}