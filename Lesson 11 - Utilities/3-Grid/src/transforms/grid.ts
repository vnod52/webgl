import { Transform } from "./transform";
import { vec3 } from "gl-matrix";
import { MAIN } from "../main";
import { ShaderGrid } from "../shaders/shaderGrid";

export class Grid extends Transform {
    constructor() {
        super(MAIN.WEBGL_APP.shaderGrid, vec3.fromValues(0,0,0), vec3.fromValues(0,0,0), vec3.fromValues(1,1,1));
        
        this.dimensions++;
    }

    dimensions: number = 12;

    update() {
        this.applyTransforms();
        this.draw();
    }

    draw() {
        MAIN.GL.useProgram(MAIN.WEBGL_APP.shaderGrid.program);
        MAIN.GL.uniform1i((<ShaderGrid>this.shader).propertyLocationDimensions, this.dimensions);
        MAIN.GL.drawArrays(MAIN.GL.POINTS, 0, Math.pow(this.dimensions, 2));
    }
}