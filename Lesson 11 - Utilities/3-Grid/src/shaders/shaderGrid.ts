import { Shader } from "./shader";
import { MAIN } from "../main";

const VERTEX_SOURCE = 
    `#version 300 es

    uniform mat4 modelMatrix;
    uniform mat4 viewMatrix;
    uniform mat4 projectionMatrix;
    uniform int dimensions;

    out vec3 linkedColor;

    void main() {
        int d = dimensions;

        vec3 pos = vec3(gl_VertexID % d, 0, gl_VertexID / d);

        pos.x -= (float(d) / 2.0) - .5;      
        pos.z -= (float(d) / 2.0) - .5;
        
        gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(pos, 1);
        gl_PointSize = 5.0;
        linkedColor = vec3(float(gl_VertexID) / pow(float(d), 2.0), 0.2, 0.2);
    }`
;
const FRAGMENT_SOURCE = 
    `#version 300 es
    precision mediump float;

    in vec3 linkedColor;

    out vec4 outColor;

    void main() {
        outColor = vec4(linkedColor, 1);
    }`
;

export class ShaderGrid extends Shader {
    constructor() {
        super(VERTEX_SOURCE, FRAGMENT_SOURCE);
        
        this.getPropertyLocations();
    }
    
    propertyLocationDimensions: WebGLUniformLocation;
    
    getPropertyLocations() {
        super.getPropertyLocations();
        
        MAIN.GL.useProgram(this.program);
    
        this.propertyLocationDimensions = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program, "dimensions");
    }
}