import { SceneObject } from "./transforms/sceneObject";
import { Camera } from "./camera";
import { Mesh } from "./mesh";
import { TextureManager } from "./textureManager";
import { MAIN } from "./main";
import { vec3 } from "gl-matrix";
import { AmbientLight } from "./transforms/ambientLight";
import { Input } from "./input";
import { MathUtils } from "./utilities/mathUtils";
import { ShaderLightGizmo } from "./shaders/shaderLightGizmo";
import { ShaderObject } from "./shaders/shaderObject";
import { Time } from "./utilities/time";
import { Grid } from "./transforms/grid";
import { ShaderGrid } from "./shaders/shaderGrid";

export class WebGLApp {
    constructor() {
    }

    cube: Mesh;
    pyramid: Mesh;
    sceneObjects: SceneObject[];
    grid: Grid;
    textureManager: TextureManager;
    shaderLightGizmo: ShaderLightGizmo;
    shaderObject: ShaderObject;
    shaderGrid: ShaderGrid;
    camera: Camera;
    ambientLight: AmbientLight;
    input = new Input();
    mathUtils: MathUtils = new MathUtils();
    isLocked: boolean = false;
    time: Time = new Time(false);

    initialize() {
        this.lockCursor();
        this.setWebGLPreferences();

        this.shaderLightGizmo = new ShaderLightGizmo();
        this.shaderObject = new ShaderObject();
        this.shaderGrid = new ShaderGrid();
        this.cube = new Mesh(Mesh.CUBE);
        this.pyramid = new Mesh(Mesh.PYRAMID);
        this.camera = new Camera();
        this.ambientLight = new AmbientLight();
        this.grid = new Grid();

        this.sceneObjects = [
        //     new SceneObject(this.cube,      vec3.fromValues(0, 0, -3), vec3.fromValues(179, 0, 0), vec3.fromValues(0.5, 0.5, 0.5))
        // ,   new SceneObject(this.pyramid,   vec3.fromValues(1, 0, -3), vec3.fromValues(179, 0, 0))
        // ,   new SceneObject(this.cube,      vec3.fromValues(-1, 0, -3))
            new SceneObject(this.cube,      vec3.fromValues(0, 0, 0.5))
        ,   new SceneObject(this.pyramid,   vec3.fromValues(2, 2, 2))
        ,   new SceneObject(this.cube,      vec3.fromValues(1, 0, 0.5))
        ];
        this.textureManager = new TextureManager();

        requestAnimationFrame(() => { this.update() });
    }

    setWebGLPreferences() {
        MAIN.GL.viewport(0, 0, MAIN.CANVAS.clientWidth, MAIN.CANVAS.clientHeight);
        MAIN.GL.clearColor(0, 0, 0, 1);
        MAIN.GL.enable(MAIN.GL.DEPTH_TEST);
    }

    update() {
        if (!this.textureManager.isReady()) {
            requestAnimationFrame(() => { this.update() });
            return;
        }

        this.draw();

        this.grid.update();
        this.time.update();
        this.camera.update();

        this.sceneObjects.forEach(e => {
            e.update();
        });

        this.ambientLight.update();
        this.input.reset();

        requestAnimationFrame(() => { this.update() });
    }

    draw() {
        MAIN.GL.clear(MAIN.GL.COLOR_BUFFER_BIT | MAIN.GL.DEPTH_BUFFER_BIT);
    }

    lockCursor() {
        MAIN.CANVAS.onclick = () => {
            MAIN.CANVAS.requestPointerLock();
        }

        MAIN.CANVAS.requestPointerLock();

        document.addEventListener('pointerlockchange', () => {
            this.isLocked = (document.pointerLockElement == MAIN.CANVAS);
        }, false);
    }
}