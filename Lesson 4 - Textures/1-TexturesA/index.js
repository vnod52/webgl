const CANVAS = document.getElementById("render-canvas");
const GL = CANVAS.getContext("webgl2");
const FLOAT_BYTE_SIZE = 4;
const STRIDE = 9 * FLOAT_BYTE_SIZE;
const VERTEX_COLOR_OFFSET = 3 * FLOAT_BYTE_SIZE;
const VERTEX_TEX_COORD_OFFSET = 7 * FLOAT_BYTE_SIZE;

var shader = {
    program: null,
    propertyLocationPosition: 0,
    propertyLocationPointSize: 0,
    propertyLocationColor: 0,
    propertyLocationTexCoord: 0,
    propertyLocationMainTexture: 0,

    vertexSource:
    `#version 300 es

    in vec4 position;
    in vec4 color;
    in float pointSize;
    in vec2 texCoord;

    out vec4 linkedColor;
    out vec2 linkedTexCoord;

    void main(){
        gl_Position = position;
        gl_PointSize = pointSize;
        linkedColor = color;
        linkedTexCoord = texCoord;
    }`,

    fragmentSource:
    `#version 300 es
    precision mediump float;

    in vec4 linkedColor;
    in vec2 linkedTexCoord;

    uniform sampler2D mainTexture;

    out vec4 outColor;

    void main(){
        // outColor = texture(mainTexture, linkedTexCoord) * linkedColor;
        outColor = linkedColor;
    }`

};

var vertexDataBuffer;
var textureBufferA = GL.createTexture();
textureBufferA.image = new Image();
textureBufferA.image.src = "./images/texture.png";

//Check if texture is loaded first before running methods
textureBufferA.image.onload = function() {
    GL.bindTexture(GL.TEXTURE_2D, textureBufferA);
    GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGB, GL.RGB, GL.UNSIGNED_BYTE, textureBufferA.image);

    GL.textParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);
    GL.textParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST);

    console.log(textureBufferA.image.complete);

    start();
}

start();



//Uses client values to determine canvas size.
function start(){
    //Create the size of the GL canvas
    GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);

    //Change the background color of the canvas
    GL.clearColor(0, 0, 0, 1);

    createVertices();

    shader.program = createShader();
    getPropertyLocation();
    setShaderProperties();
    
    draw();
    
}

function draw(){
    GL.clear(GL.COLOR_BUFFER_BIT);
    GL.drawArrays(GL.TRIANGLES, 0, 3);
    // GL.drawArrays(GL.TRIANGLES, 5, 10);
}

function createShader() {
    var vertexShader = getAndCompileShader(shader.vertexSource, GL.VERTEX_SHADER);
    var fragmentShader = getAndCompileShader(shader.fragmentSource,GL.FRAGMENT_SHADER);

    var newShaderProgram = GL.createProgram();
    GL.attachShader(newShaderProgram, vertexShader);
    GL.attachShader(newShaderProgram, fragmentShader);
    GL.linkProgram(newShaderProgram);
    GL.useProgram(shader.program);

    return newShaderProgram
}

function getAndCompileShader(shaderSource, shaderType) {
    var newShader = GL.createShader(shaderType);
    GL.shaderSource(newShader, shaderSource);
    GL.compileShader(newShader);

    console.log(shaderSource);

    if (!GL.getShaderParameter(newShader, GL.COMPILE_STATUS)){
        alert(GL.getShaderInfoLog(newShader));
        return null;
    }

    return newShader
}

function getPropertyLocation(){
    shader.propertyLocationPosition    = GL.getAttribLocation(shader.program, "position");
    shader.propertyLocationPointSize   = GL.getAttribLocation(shader.program, "pointSize");
    shader.propertyLocationColor       = GL.getAttribLocation(shader.program, "color");
    shader.propertyLocationtexCoord    = GL.getAttribLocation(shader.program, "texCoord");
    shader.propertyLocationMainTexture = GL.getUniformLocation(shader.program, "mainTexture");
    

    GL.enableVertexAttribArray(shader.propertyLocationPosition);
    GL.enableVertexAttribArray(shader.propertyLocationColor);
    GL.enableVertexAttribArray(shader.propertyLocationTexCoord);

}

function setShaderProperties() {
    GL.useProgram(shader.program);

    GL.vertexAttribPointer(shader.propertyLocationPosition, 3, GL.FLOAT, false, STRIDE, 0);
    GL.vertexAttribPointer(shader.propertyLocationColor, 4, GL.FLOAT, false, STRIDE, VERTEX_COLOR_OFFSET);
    GL.vertexAttribPointer(shader.propertyLocationTexCoord, 2, GL.FLOAT, false, STRIDE, VERTEX_TEX_COORD_OFFSET);

    GL.vertexAttrib1f(shader.propertyLocationPointSize, 50);

    GL.activeTexture(GL.TEXTURE0);
    GL.bindTexture(GL.TEXTURE_2D, textureBufferA);
    GL.uniform1i(shader.propertyLocationMainTexture, 0);
}

function createVertices() {
    var vertices = [
        -0.5, -0.5, 0.0,   1.0, 0.0, 0.0, 1.0,  0.0, 0.0,    
         0.5, -0.5, 0.0,   0.0, 1.0, 0.0, 1.0,  1.0, 0.0,
        -0.0,  0.5, 0.0,   0.0, 0.0, 1.0, 1.0,  0.5, 1.0
    ];

    //Open a buffer, bind the data and send it to
    vertexDataBuffer = GL.createBuffer();
    GL.bindBuffer(GL.ARRAY_BUFFER, vertexDataBuffer);
    GL.bufferData(GL.ARRAY_BUFFER, new Float32Array(vertices), GL.STATIC_DRAW);

}