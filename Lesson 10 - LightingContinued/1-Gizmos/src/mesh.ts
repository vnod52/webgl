import { MAIN } from "./main";

const FLOAT_BYTE_SIZE: number = 4;
const STRIDE: number = 9 * FLOAT_BYTE_SIZE;
const VERTEX_COLOR_OFFSET: number = 3 * FLOAT_BYTE_SIZE;
const VERTEX_TEX_COORD_OFFSET: number = 7 * FLOAT_BYTE_SIZE;

export class Mesh {
    constructor() {
        this.createVertices();
        this.setShaderProperties();
    }

    vertexBuffer: WebGLBuffer;
    vertices: number[] = [
        -1.0, -0.5, -1.0,   1.0, 0.0, 0.0, 1.0,    0.0, 0.0, //back
         1.0, -0.5, -1.0,   0.0, 1.0, 0.0, 1.0,    1.0, 0.0,
         0.0,  1.0,  0.0,   0.0, 0.0, 1.0, 1.0,    0.5, 1.0,

        -1.0, -0.5,  1.0,   0.0, 1.0, 0.0, 1.0,    0.0, 0.0, //forward
         1.0, -0.5,  1.0,   1.0, 0.0, 0.0, 1.0,    1.0, 0.0,
         0.0,  1.0,  0.0,   0.0, 0.0, 1.0, 1.0,    0.5, 1.0,

        -1.0, -0.5, -1.0,   1.0, 0.0, 0.0, 1.0,    0.0, 0.0, //left
        -1.0, -0.5,  1.0,   0.0, 1.0, 0.0, 1.0,    1.0, 0.0,
         0.0,  1.0,  0.0,   0.0, 0.0, 1.0, 1.0,    0.5, 1.0,

         1.0, -0.5,  1.0,   1.0, 0.0, 0.0, 1.0,    0.0, 0.0, //right
         1.0, -0.5, -1.0,   0.0, 1.0, 0.0, 1.0,    1.0, 0.0,
         0.0,  1.0,  0.0,   0.0, 0.0, 1.0, 1.0,    0.5, 1.0,

        -1.0, -0.5, -1.0,   1.0, 0.0, 0.0, 1.0,    0.0, 0.0, //down
        -1.0, -0.5,  1.0,   0.0, 1.0, 0.0, 1.0,    0.0, 1.0,
         1.0, -0.5,  1.0,   1.0, 0.0, 0.0, 1.0,    1.0, 1.0,

        -1.0, -0.5, -1.0,   1.0, 0.0, 0.0, 1.0,    0.0, 0.0, //down
         1.0, -0.5,  1.0,   1.0, 0.0, 0.0, 1.0,    1.0, 1.0,
         1.0, -0.5, -1.0,   0.0, 1.0, 0.0, 1.0,    1.0, 0.0,
    ];

    createVertices() {    
        this.vertexBuffer = <WebGLBuffer>MAIN.GL.createBuffer();
        MAIN.GL.bindBuffer(MAIN.GL.ARRAY_BUFFER, this.vertexBuffer);
        MAIN.GL.bufferData(MAIN.GL.ARRAY_BUFFER, new Float32Array(this.vertices), MAIN.GL.STATIC_DRAW);
    }

    setShaderProperties() {
        var shader = MAIN.WEBGL_APP.shaderObject;
        MAIN.GL.useProgram(shader.program);

        MAIN.GL.vertexAttribPointer(shader.propertyLocationPosition, 3, MAIN.GL.FLOAT, false, STRIDE, 0);
        MAIN.GL.vertexAttribPointer(shader.propertyLocationColor, 4, MAIN.GL.FLOAT, false, STRIDE, VERTEX_COLOR_OFFSET);
        MAIN.GL.vertexAttribPointer(shader.propertyLocationTexCoord, 2, MAIN.GL.FLOAT, false, STRIDE, VERTEX_TEX_COORD_OFFSET);

        MAIN.GL.vertexAttrib1f(shader.propertyLocationPointSize, 50);
    }
}