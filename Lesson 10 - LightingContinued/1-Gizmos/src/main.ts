import { WebGLApp } from "./webGLApp";

const CANVAS: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("render-canvas");
// const GL = CANVAS.getContext("webgl2");
const GL: WebGL2RenderingContext = <WebGL2RenderingContext>CANVAS.getContext("webgl2", {preserveDrawingBuffer: true});
const WEBGL_APP: WebGLApp  = new WebGLApp();

export const MAIN = {
    CANVAS,
    GL,
    WEBGL_APP
}

WEBGL_APP.initialize();