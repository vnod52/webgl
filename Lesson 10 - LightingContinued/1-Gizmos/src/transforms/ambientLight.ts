import { vec3 } from "gl-matrix";
import { MAIN } from "../main";
import { Transform } from "./transform";

export class AmbientLight extends Transform {
    constructor(_position: vec3 = vec3.fromValues(0,0,0), _rotation: vec3 = vec3.fromValues(0,0,0), _scale: vec3 = vec3.fromValues(1,1,1)) {
        super(MAIN.WEBGL_APP.shaderLightGizmo, _position, _rotation, _scale);
        
        this.updateAmbientLight();
    }

    color: vec3 = vec3.fromValues(0.4, 0.4, 1);
    intensity: number = 1;

    updateAmbientLight() {
        // Use the object shader for sending light.
        MAIN.GL.useProgram(MAIN.WEBGL_APP.shaderObject.program);
        MAIN.GL.uniform3fv(MAIN.WEBGL_APP.shaderObject.propertyLocationAmbientColor, this.color);
        MAIN.GL.uniform1f(MAIN.WEBGL_APP.shaderObject.propertyLocationAmbientIntensity, this.intensity);

        // Use the light gizmo shader for sending colour to our gizmo.
        MAIN.GL.useProgram(MAIN.WEBGL_APP.shaderLightGizmo.program);
        MAIN.GL.uniform3fv(MAIN.WEBGL_APP.shaderLightGizmo.propertyLocationColor, this.color);
        MAIN.GL.uniform1f(MAIN.WEBGL_APP.shaderLightGizmo.propertyLocationIntensity, this.intensity);
    }

    timer: number = 0;

    update() {
        this.timer += 0.1;
        this.scale = vec3.fromValues(0.2, 0.2, 0.2)
        this.position[0] = Math.sin(this.timer);
        this.position[1] = Math.cos(this.timer);

        this.applyTransforms();
        this.draw();
    }

    draw() {
        MAIN.GL.useProgram(this.shader.program);

        MAIN.GL.bindBuffer(MAIN.GL.ARRAY_BUFFER, MAIN.WEBGL_APP.mesh.vertexBuffer);
        MAIN.GL.drawArrays(MAIN.GL.TRIANGLES, 0, 18);
    }
}