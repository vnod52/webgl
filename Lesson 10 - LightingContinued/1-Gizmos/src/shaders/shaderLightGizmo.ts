import { Shader } from "./shader";
import { MAIN } from "../main";

const VERTEX_SOURCE: string = 
    `#version 300 es

    in vec4 position;

    uniform mat4 projectionMatrix;
    uniform mat4 viewMatrix;
    uniform mat4 modelMatrix;

    void main() {
        gl_Position = projectionMatrix * viewMatrix * modelMatrix * position;
    }`
;
const FRAGMENT_SOURCE: string = 
    `#version 300 es
    precision mediump float;

    uniform vec3 ambientColor;
    uniform float ambientIntensity;

    out vec4 outColor;

    void main() {
        outColor = vec4(ambientColor * ambientIntensity, 1);
    }`
;

export class ShaderLightGizmo extends Shader {
    constructor() {
        super(VERTEX_SOURCE, FRAGMENT_SOURCE);
        this.getPropertyLocations();
    }

    propertyLocationColor: WebGLUniformLocation;
    propertyLocationIntensity: WebGLUniformLocation;

    getPropertyLocations() {
        MAIN.GL.useProgram(this.program);

        this.propertyLocationPosition         = MAIN.GL.getAttribLocation(this.program, "position");
        
        this.propertyLocationColor            = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program, "ambientColor");
        this.propertyLocationIntensity        = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program, "ambientIntensity");
        this.propertyLocationModelMatrix      = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program, "modelMatrix");
        this.propertyLocationProjectionMatrix = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program, "projectionMatrix");
        this.propertyLocationViewMatrix       = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program, "viewMatrix");

        MAIN.GL.enableVertexAttribArray(this.propertyLocationPosition);
    }
}