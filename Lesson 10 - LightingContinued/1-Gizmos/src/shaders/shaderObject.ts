import { Shader } from "./shader";
import { MAIN } from "../main";

const VERTEX_SOURCE: string = 
    `#version 300 es

    in vec4 position;
    in vec4 color;
    in float pointSize;
    in vec2 texCoord;

    uniform mat4 projectionMatrix;
    uniform mat4 viewMatrix;
    uniform mat4 modelMatrix;

    out vec4 linkedColor;
    out vec2 linkedTexCoord;
    
    void main() {
        gl_Position = projectionMatrix * viewMatrix * modelMatrix * position;
        gl_PointSize = pointSize;
        linkedColor = color;
        linkedTexCoord = texCoord;
    }`
;
const FRAGMENT_SOURCE: string = 
    `#version 300 es
    precision mediump float;

    in vec4 linkedColor;
    in vec2 linkedTexCoord;

    uniform sampler2D texture0;
    uniform sampler2D texture1;
    uniform vec3 ambientColor;
    uniform float ambientIntensity;

    out vec4 outColor;

    void main() {
        outColor = mix(texture(texture0, linkedTexCoord), texture(texture1, linkedTexCoord), 0.5) * linkedColor * vec4(ambientColor * ambientIntensity, 1.0);
        // outColor = linkedColor * vec4(ambientColor * ambientIntensity, 1.0);
    }`
;

export class ShaderObject extends Shader {
    constructor() {
        super(VERTEX_SOURCE, FRAGMENT_SOURCE);
        this.getPropertyLocations();
    }

    propertyLocationPointSize: number;
    propertyLocationColor: number;
    propertyLocationTexCoord: number;
    propertyLocationTextures: WebGLUniformLocation[] = [];
    propertyLocationAmbientColor    : WebGLUniformLocation;
    propertyLocationAmbientIntensity: WebGLUniformLocation;

    getPropertyLocations() {
        MAIN.GL.useProgram(this.program);

        this.propertyLocationPosition         = MAIN.GL.getAttribLocation(this.program   , "position");
        this.propertyLocationPointSize        = MAIN.GL.getAttribLocation(this.program   , "pointSize");
        this.propertyLocationColor            = MAIN.GL.getAttribLocation(this.program   , "color");
        this.propertyLocationTexCoord         = MAIN.GL.getAttribLocation(this.program   , "texCoord");

        this.propertyLocationAmbientColor     = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "ambientColor");
        this.propertyLocationAmbientIntensity = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "ambientIntensity");
        this.propertyLocationModelMatrix      = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "modelMatrix");
        this.propertyLocationProjectionMatrix = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "projectionMatrix");
        this.propertyLocationViewMatrix       = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "viewMatrix");
        this.propertyLocationTextures = [
            <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "texture0")
        ,   <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "texture1")
        ]

        MAIN.GL.enableVertexAttribArray(this.propertyLocationPosition);
        MAIN.GL.enableVertexAttribArray(this.propertyLocationColor);
        MAIN.GL.enableVertexAttribArray(this.propertyLocationTexCoord);
    }
}