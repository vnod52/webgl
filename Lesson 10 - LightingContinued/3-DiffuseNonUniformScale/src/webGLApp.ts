import { SceneObject } from "./transforms/sceneObject";
import { Camera } from "./camera";
import { Mesh } from "./mesh";
import { TextureManager } from "./textureManager";
import { MAIN } from "./main";
import { vec3 } from "gl-matrix";
import { AmbientLight } from "./transforms/ambientLight";
import { Input } from "./input";
import { MathUtils } from "./utilities/mathUtils";
import { ShaderLightGizmo } from "./shaders/shaderLightGizmo";
import { ShaderObject } from "./shaders/shaderObject";

export class WebGLApp {
    constructor() {
    }

    mesh: Mesh;
    sceneObjects: SceneObject[];
    textureManager: TextureManager;
    shaderLightGizmo: ShaderLightGizmo;
    shaderObject: ShaderObject;
    camera: Camera;
    ambientLight: AmbientLight;
    input = new Input();
    mathUtils: MathUtils = new MathUtils();
    isLocked: boolean = false;

    initialize() {
        this.lockCursor();
        this.setWebGLPreferences();

        this.shaderLightGizmo = new ShaderLightGizmo();
        this.shaderObject = new ShaderObject();
        this.mesh = new Mesh();
        this.camera = new Camera();
        this.ambientLight = new AmbientLight();

        this.sceneObjects = [
            new SceneObject(vec3.fromValues(0, 0, 1), vec3.fromValues(45, 45, 0), vec3.fromValues(0.5, 0.5, 0.5))
        ,   new SceneObject(vec3.fromValues(0.75, 0, 1), vec3.fromValues(0, 0, 90))
        ,   new SceneObject(vec3.fromValues(-0.75, 0, 1))
        ];
        this.textureManager = new TextureManager();

        requestAnimationFrame(() => { this.update() });
    }

    setWebGLPreferences() {
        MAIN.GL.viewport(0, 0, MAIN.CANVAS.clientWidth, MAIN.CANVAS.clientHeight);
        MAIN.GL.clearColor(0, 0, 0, 1);
        MAIN.GL.enable(MAIN.GL.DEPTH_TEST);
    }

    update() {
        if (!this.textureManager.isReady()) {
            requestAnimationFrame(() => { this.update() });
            return;
        }

        this.draw();
        this.camera.update();

        this.sceneObjects.forEach(e => {
            e.update();
        });

        this.ambientLight.update();

        this.input.reset();

        requestAnimationFrame(() => { this.update() });
    }

    draw() {
        MAIN.GL.clear(MAIN.GL.COLOR_BUFFER_BIT | MAIN.GL.DEPTH_BUFFER_BIT);
    }

    lockCursor() {
        MAIN.CANVAS.onclick = () => {
            MAIN.CANVAS.requestPointerLock();
        }

        MAIN.CANVAS.requestPointerLock();

        document.addEventListener('pointerlockchange', () => {
            this.isLocked = (document.pointerLockElement == MAIN.CANVAS);
        }, false);
    }
}