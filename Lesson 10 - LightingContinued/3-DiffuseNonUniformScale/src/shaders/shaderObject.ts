import { Shader } from "./shader";
import { MAIN } from "../main";

const VERTEX_SOURCE: string = 
    `#version 300 es

    in vec4 position;
    in vec4 color;
    in float pointSize;
    in vec2 texCoord;
    in vec3 vertexNormal;

    uniform mat4 projectionMatrix;
    uniform mat4 viewMatrix;
    uniform mat4 modelMatrix;

    out vec4 linkedColor;
    out vec2 linkedTexCoord;
    out vec4 linkedVertexWorldPosition;
    out vec3 linkedVertexNormal;
    
    void main() {
        gl_Position = projectionMatrix * viewMatrix * modelMatrix * position;
        gl_PointSize = pointSize;
        linkedColor = color;
        linkedTexCoord = texCoord;
        linkedVertexWorldPosition = modelMatrix * position;
        linkedVertexNormal = mat3(inverse(transpose(modelMatrix))) * vertexNormal;
    }`
;
const FRAGMENT_SOURCE: string = 
    `#version 300 es
    precision mediump float;

    in vec4 linkedColor;
    in vec2 linkedTexCoord;
    in vec4 linkedVertexWorldPosition;
    in vec3 linkedVertexNormal;

    uniform sampler2D texture0;
    uniform sampler2D texture1;
    uniform vec3 ambientColor;
    uniform float ambientIntensity;
    uniform vec3 lightPosition;

    out vec4 outColor;

    void main() {
        vec3 lightDirection = normalize(lightPosition - vec3(linkedVertexWorldPosition));
        float diffuseIntensity = max(dot(normalize(linkedVertexNormal), lightDirection), 0.0);
        vec4 diffuseComponent = vec4((diffuseIntensity * 2.0) * ambientColor, 1.0);

        vec4 ambientComponent = vec4(ambientColor * ambientIntensity, 1.0);

        vec4 colorComponent = mix(texture(texture0, linkedTexCoord), texture(texture1, linkedTexCoord), 0.5) * linkedColor;

        outColor = colorComponent * (ambientComponent + diffuseComponent);
    }`
;

export class ShaderObject extends Shader {
    constructor() {
        super(VERTEX_SOURCE, FRAGMENT_SOURCE);
        this.getPropertyLocations();
    }

    propertyLocationPointSize: number;
    propertyLocationColor: number;
    propertyLocationTexCoord: number;
    propertyLocationVertexNormal: number;

    propertyLocationLightPosition: WebGLUniformLocation;
    propertyLocationTextures: WebGLUniformLocation[] = [];
    propertyLocationAmbientColor    : WebGLUniformLocation;
    propertyLocationAmbientIntensity: WebGLUniformLocation;

    getPropertyLocations() {
        MAIN.GL.useProgram(this.program);


        this.propertyLocationPosition         = MAIN.GL.getAttribLocation(this.program, "position");
        this.propertyLocationPointSize        = MAIN.GL.getAttribLocation(this.program, "pointSize");
        this.propertyLocationColor            = MAIN.GL.getAttribLocation(this.program, "color");
        this.propertyLocationTexCoord         = MAIN.GL.getAttribLocation(this.program, "texCoord");
        this.propertyLocationVertexNormal     = MAIN.GL.getAttribLocation(this.program, "vertexNormal");

        this.propertyLocationLightPosition     = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "lightPosition");
        this.propertyLocationAmbientColor      = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "ambientColor");
        this.propertyLocationAmbientIntensity  = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "ambientIntensity");
        this.propertyLocationModelMatrix       = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "modelMatrix");
        this.propertyLocationProjectionMatrix  = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "projectionMatrix");
        this.propertyLocationViewMatrix        = <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "viewMatrix");
        this.propertyLocationTextures = [
            <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "texture0")
        ,   <WebGLUniformLocation>MAIN.GL.getUniformLocation(this.program  , "texture1")
        ]

        MAIN.GL.enableVertexAttribArray(this.propertyLocationPosition);
        MAIN.GL.enableVertexAttribArray(this.propertyLocationColor);
        MAIN.GL.enableVertexAttribArray(this.propertyLocationTexCoord);
        MAIN.GL.enableVertexAttribArray(this.propertyLocationVertexNormal);

        console.log(this);
    }
}