class Input {
    constructor() {
        this.setKeyboardListener();
    }

    mouseDelta = vec2.create();
    pressed = {
        w: false
        , a: false
        , s: false
        , d: false
        // up/down
        , q: false
        , e: false
    }

    setKeyboardListener() {
        document.addEventListener("keydown", (e) => {
            console.log(e.keyCode);
            if (e.key == 'd')
                this.pressed.d = true;
            if (e.key == 'a')
                this.pressed.a = true;
            if (e.key == 'w')
                this.pressed.w = true;
            if (e.key == 's')
                this.pressed.s = true;
            // up/down
            if (e.key == 'q')
                this.pressed.q = true;
            if (e.key == 'e')
                this.pressed.e = true;
        });

        document.addEventListener("keyup", (e) => {
            if (e.key == 'd')
                this.pressed.d = false;
            if (e.key == 'a')
                this.pressed.a = false;
            if (e.key == 'w')
                this.pressed.w = false;
            if (e.key == 's')
                this.pressed.s = false;
            // up/down
            if (e.key == 'q')
                this.pressed.q = false;
            if (e.key == 'e')
                this.pressed.e = false;
        });

        document.addEventListener("mousemove", (e) => {
            this.mouseDelta = vec2.fromValues(e.movementX, e.movementY);
        });
    }

    reset() {
        // console.log(this.mouseDelta);
        this.mouseDelta = vec2.fromValues(0, 0);
    }
}