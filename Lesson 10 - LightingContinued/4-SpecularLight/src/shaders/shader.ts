import { MAIN } from "../main";

export class Shader {
    constructor(sourceVertex: string, sourceFragment: string) {
        this.program = <WebGLProgram>this.getNewShaderProgram(sourceVertex, sourceFragment);
    }

    program: WebGLProgram;
    propertyLocationPosition: number;
    propertyLocationProjectionMatrix: WebGLUniformLocation;
    propertyLocationViewMatrix: WebGLUniformLocation;
    propertyLocationModelMatrix: WebGLUniformLocation;
    

    getNewShaderProgram(sourceVertex: string, sourceFragment: string): WebGLProgram {
        var vertexShader: WebGLShader = <WebGLShader>this.getAndCompileShader(sourceVertex, MAIN.GL.VERTEX_SHADER);
        var fragmentShader: WebGLShader = <WebGLShader>this.getAndCompileShader(sourceFragment, MAIN.GL.FRAGMENT_SHADER);
    
        var newShaderProgram: WebGLProgram = <WebGLProgram>MAIN.GL.createProgram();
        MAIN.GL.attachShader(newShaderProgram, vertexShader);
        MAIN.GL.attachShader(newShaderProgram, fragmentShader);
        MAIN.GL.linkProgram(newShaderProgram);
        MAIN.GL.useProgram(newShaderProgram);
    
        return newShaderProgram;
    }

    getAndCompileShader(shaderSource: string, shaderType: number): WebGLShader {
        var newShader: WebGLShader = <WebGLShader>MAIN.GL.createShader(shaderType);
        MAIN.GL.shaderSource(newShader, shaderSource);
        MAIN.GL.compileShader(newShader);
    
        console.log(shaderSource);
    
        if (!MAIN.GL.getShaderParameter(newShader, MAIN.GL.COMPILE_STATUS)) {
            alert(MAIN.GL.getShaderInfoLog(newShader));
        }
    
        return newShader;
    }
}