import { MAIN } from "./main";

export class TextureManager {
    constructor() {
        this.textureBuffers = [
            this.createTexture("./images/Clouds.png"),
            this.createTexture("./images/Beagle.jpg")
        ];
    }

    textureBuffers: WebGLTexture[] = [];
    readyCount = 0;

    createTexture(_url: string): WebGLTexture {
        var textureBuffer: WebGLTexture = <WebGLTexture>MAIN.GL.createTexture();
        var image: any = new Image();
        image.src = _url;
        image.onload = () => {
            MAIN.GL.bindTexture(MAIN.GL.TEXTURE_2D, textureBuffer);
            MAIN.GL.pixelStorei(MAIN.GL.UNPACK_FLIP_Y_WEBGL, true);
            MAIN.GL.texImage2D(MAIN.GL.TEXTURE_2D, 0, MAIN.GL.RGB, MAIN.GL.RGB, MAIN.GL.UNSIGNED_BYTE, image);
        
            MAIN.GL.texParameteri(MAIN.GL.TEXTURE_2D, MAIN.GL.TEXTURE_MAG_FILTER, MAIN.GL.NEAREST);
            MAIN.GL.texParameteri(MAIN.GL.TEXTURE_2D, MAIN.GL.TEXTURE_MIN_FILTER, MAIN.GL.NEAREST);
        
            this.setIsReady();
        }

        return textureBuffer;
    }

    isReady() {
        return this.textureBuffers.length == this.readyCount;
    }

    setIsReady() {
        if (this.textureBuffers.length == ++this.readyCount) {
            this.setShaderProperties();
        }
    }

    setShaderProperties() {
        var shader = MAIN.WEBGL_APP.shaderObject;
        MAIN.GL.useProgram(shader.program);

        for (let i = 0; i < this.textureBuffers.length; i++) {
            MAIN.GL.activeTexture(MAIN.GL.TEXTURE0 + i);
            MAIN.GL.bindTexture(MAIN.GL.TEXTURE_2D, this.textureBuffers[i]);
            MAIN.GL.uniform1i(shader.propertyLocationTextures[i], i);
        }
    }
}