import { Transform } from "./transform";
import { MAIN } from "../main";
import { vec3 } from "gl-matrix";

export class SceneObject extends Transform {
    constructor(_position: vec3 = vec3.fromValues(0,0,0), _rotation: vec3 = vec3.fromValues(0,0,0), _scale: vec3 = vec3.fromValues(1,1,1)) {
        super(MAIN.WEBGL_APP.shaderObject, _position, _rotation, _scale);

        this.initialPosition = _position;
    }

    angle: number       = Math.random() * (180 - -180) + -180;
    angleSpeed: number  = Math.random() * (3 - 0.5) + 0.5;
    offset: number      = 0;
    offsetSpeed: number = Math.random() * (0.1 - 0.005) + 0.005;
    initialPosition: vec3;

    update() {
        // this.transform();
        this.applyTransforms();

        this.draw();
    }

    draw() {
        MAIN.GL.useProgram(MAIN.WEBGL_APP.shaderObject.program);

        MAIN.GL.bindBuffer(MAIN.GL.ARRAY_BUFFER, MAIN.WEBGL_APP.mesh.vertexBuffer);
        MAIN.GL.drawArrays(MAIN.GL.TRIANGLES, 0, 18);
    }

    transform() {
        this.offset += this.offsetSpeed;
        this.position = vec3.fromValues(
            this.initialPosition[0] + Math.sin(this.offset)
        ,   this.initialPosition[1] + Math.cos(this.offset)
        ,   -3
        );
        
        this.angle += (Math.PI * 2) * (0.5 / 360) * this.angleSpeed;
        this.rotation = vec3.fromValues(
            this.angle
        ,   this.angle * 2
        ,   0
        )

        this.scale = vec3.fromValues(
            Math.sin(this.angle)
        ,   Math.cos(this.angle)
        ,   1
        );
    }
}