import { mat4, vec3 } from "gl-matrix";
import { MAIN } from "../main";
import { Shader } from "../shaders/shader";

export class Transform {
    constructor(_shader: Shader, _position: vec3 = vec3.fromValues(0,0,0), _rotation: vec3 = vec3.fromValues(0,0,0), _scale: vec3 = vec3.fromValues(1,1,1)) {
        this.shader = _shader;
        this.position = _position;
        this.rotation = _rotation;
        this.scale = _scale;
    }

    position: vec3 = vec3.fromValues(0, 0, 0);
    rotation: vec3 = vec3.fromValues(0, 0, 0);
    scale: vec3    = vec3.fromValues(1, 1, 1);
    modelMatrix: mat4 = mat4.create();
    shader: Shader;

    applyTransforms() {
        mat4.identity(this.modelMatrix);
        mat4.translate(this.modelMatrix, this.modelMatrix, this.position);
        mat4.rotateZ(this.modelMatrix, this.modelMatrix, this.rotation[2]);
        mat4.rotateY(this.modelMatrix, this.modelMatrix, this.rotation[1]);
        mat4.rotateX(this.modelMatrix, this.modelMatrix, this.rotation[0]);
        mat4.scale(this.modelMatrix, this.modelMatrix, this.scale);

        MAIN.GL.useProgram(this.shader.program);
        MAIN.GL.uniformMatrix4fv(this.shader.propertyLocationModelMatrix, false, this.modelMatrix);
    }
}