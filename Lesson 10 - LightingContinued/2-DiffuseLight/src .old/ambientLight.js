class AmbientLight {
    constructor() {
        this.updateAmbientLight();
    }

    color = vec3.fromValues(0.4, 0.4, 1);
    intensity = 1;

    updateAmbientLight() {
        GL.uniform3fv(WEBGL_APP.shader.propertyLocationAmbientColor, this.color);
        console.log(WEBGL_APP.shader.propertyLocationAmbientIntensity);
        GL.uniform1f(WEBGL_APP.shader.propertyLocationAmbientIntensity, this.intensity);
    }
}