"use strict";
var travis = {
    name: 'Travis',
    age: 22,
    title: 'Proud cat owner'
};
var welcomePerson = function (somebody) {
    return "Welcome, " + somebody.name + ", you're " + somebody.age + " & a " + somebody.title + ".";
};
console.log(welcomePerson(travis));
