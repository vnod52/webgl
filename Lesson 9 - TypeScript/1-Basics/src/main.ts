interface Person {
    name: string;
    age: number;
    title: string;
}

let travis = {
    name : 'Travis',
    age: 22,
    title: 'Proud cat owner'
}

let welcomePerson = (somebody: Person) => {
    return `Welcome, ${somebody.name}, you're ${somebody.age} & a ${somebody.title}.`
}

console.log(welcomePerson(travis));