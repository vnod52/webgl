"use strict";
var mat4 = glMatrix.mat4, vec2 = glMatrix.vec2, vec3 = glMatrix.vec3;
var CANVAS = document.getElementById("render-canvas");
// const GL = CANVAS.getContext("webgl2");
var GL = CANVAS.getContext("webgl2", { preserveDrawingBuffer: true });
var WEBGL_APP = new WebGLApp();
WEBGL_APP.initialize();
