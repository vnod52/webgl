"use strict";
var AmbientLight = /** @class */ (function () {
    function AmbientLight() {
        this.color = vec3.fromValues(0.4, 0.4, 1);
        this.intensity = 1;
        this.updateAmbientLight();
    }
    AmbientLight.prototype.updateAmbientLight = function () {
        GL.uniform3fv(WEBGL_APP.shader.propertyLocationAmbientColor, this.color);
        GL.uniform1f(WEBGL_APP.shader.propertyLocationAmbientIntensity, this.intensity);
    };
    return AmbientLight;
}());
