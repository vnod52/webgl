"use strict";
var Input = /** @class */ (function () {
    function Input() {
        this.mouseDelta = vec2.create();
        this.pressed = {
            w: false,
            a: false,
            s: false,
            d: false
            // up/down
            ,
            q: false,
            e: false
        };
        this.setKeyboardListener();
    }
    Input.prototype.setKeyboardListener = function () {
        var _this = this;
        document.addEventListener("keydown", function (e) {
            console.log(e.keyCode);
            if (e.key == 'd')
                _this.pressed.d = true;
            if (e.key == 'a')
                _this.pressed.a = true;
            if (e.key == 'w')
                _this.pressed.w = true;
            if (e.key == 's')
                _this.pressed.s = true;
            // up/down
            if (e.key == 'q')
                _this.pressed.q = true;
            if (e.key == 'e')
                _this.pressed.e = true;
        });
        document.addEventListener("keyup", function (e) {
            if (e.key == 'd')
                _this.pressed.d = false;
            if (e.key == 'a')
                _this.pressed.a = false;
            if (e.key == 'w')
                _this.pressed.w = false;
            if (e.key == 's')
                _this.pressed.s = false;
            // up/down
            if (e.key == 'q')
                _this.pressed.q = false;
            if (e.key == 'e')
                _this.pressed.e = false;
        });
        document.addEventListener("mousemove", function (e) {
            _this.mouseDelta = vec2.fromValues(e.movementX, e.movementY);
        });
    };
    Input.prototype.reset = function () {
        // console.log(this.mouseDelta);
        this.mouseDelta = vec2.fromValues(0, 0);
    };
    return Input;
}());
