"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var SceneObject = /** @class */ (function (_super) {
    __extends(SceneObject, _super);
    function SceneObject(_position, _rotation, _scale) {
        if (_position === void 0) { _position = { x: 0, y: 0, z: 0 }; }
        if (_rotation === void 0) { _rotation = { x: 0, y: 0, z: 0 }; }
        if (_scale === void 0) { _scale = { x: 1, y: 1, z: 1 }; }
        var _this = _super.call(this, _position, _rotation, _scale) || this;
        _this.angle = Math.random() * (180 - -180) + -180;
        _this.angleSpeed = Math.random() * (3 - 0.5) + 0.5;
        _this.offset = 0;
        _this.offsetSpeed = Math.random() * (0.1 - 0.005) + 0.005;
        _this.initialPosition = _position;
        return _this;
    }
    SceneObject.prototype.update = function () {
        this.transform();
        this.applyTransforms();
        this.draw();
    };
    SceneObject.prototype.draw = function () {
        GL.bindBuffer(GL.ARRAY_BUFFER, WEBGL_APP.mesh.vertexBuffer);
        GL.drawArrays(GL.TRIANGLES, 0, 3);
    };
    SceneObject.prototype.transform = function () {
        this.offset += this.offsetSpeed;
        this.position = {
            x: this.initialPosition.x + Math.sin(this.offset),
            y: this.initialPosition.y + Math.cos(this.offset),
            z: -3
        };
        // Output desired angle increment in radians.
        this.angle += (Math.PI * 2) * (0.5 / 360) * this.angleSpeed;
        this.rotation = {
            x: this.angle,
            y: this.angle * 2,
            z: 0
        };
        this.scale = {
            x: Math.sin(this.angle),
            y: Math.cos(this.angle),
            z: 1
        };
    };
    return SceneObject;
}(Transform));
