"use strict";
var TextureManager = /** @class */ (function () {
    function TextureManager() {
        this.textureBuffers = [];
        this.readyCount = 0;
        this.textureBuffers = [
            this.createTexture("./images/Clouds.png"),
            this.createTexture("./images/Beagle.jpg")
        ];
    }
    TextureManager.prototype.createTexture = function (_url) {
        var _this = this;
        var textureBuffer = GL.createTexture();
        var image = new Image();
        image.src = _url;
        image.onload = function () {
            GL.bindTexture(GL.TEXTURE_2D, textureBuffer);
            GL.pixelStorei(GL.UNPACK_FLIP_Y_WEBGL, true);
            GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGB, GL.RGB, GL.UNSIGNED_BYTE, image);
            GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);
            GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST);
            _this.setIsReady();
        };
        return textureBuffer;
    };
    TextureManager.prototype.isReady = function () {
        return this.textureBuffers.length == this.readyCount;
    };
    TextureManager.prototype.setIsReady = function () {
        if (this.textureBuffers.length == ++this.readyCount) {
            this.setShaderProperties();
        }
    };
    TextureManager.prototype.setShaderProperties = function () {
        var shader = WEBGL_APP.shader;
        GL.useProgram(shader.program);
        for (var i = 0; i < this.textureBuffers.length; i++) {
            GL.activeTexture(GL.TEXTURE0 + i);
            GL.bindTexture(GL.TEXTURE_2D, this.textureBuffers[i]);
            GL.uniform1i(shader.propertyLocationTextures[i], i);
        }
    };
    return TextureManager;
}());
