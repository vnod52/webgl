"use strict";
var Camera = /** @class */ (function () {
    function Camera() {
        this.viewMatrix = mat4.create();
        this.projectionMatrix = mat4.create();
        this.near = 1;
        this.far = 200;
        this.fovRadians = 60 * Math.PI / 180;
        // We are working here.
        this.moveSpeed = 0.2;
        this.rotateSpeed = 0.005;
        this.pitch = 0;
        this.yaw = -1 * Math.PI / 2;
        this.target = vec3.create();
        this.position = vec3.create();
        this.lookDirection = vec3.fromValues(0, 0, -1);
        this.update();
    }
    Camera.prototype.update = function () {
        if (CANVAS.width != CANVAS.clientWidth || CANVAS.height != CANVAS.clientHeight) {
            this.updateViewport();
        }
        this.updateProjectionMatrix();
        this.updateViewMatrix();
    };
    Camera.prototype.updateViewport = function () {
        CANVAS.width = CANVAS.clientWidth;
        CANVAS.height = CANVAS.clientHeight;
        GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);
    };
    Camera.prototype.updateProjectionMatrix = function () {
        var aspect = CANVAS.clientWidth / CANVAS.clientHeight;
        // var projectionMatrix = this.perspective(aspect);
        mat4.perspective(this.projectionMatrix, this.fovRadians, aspect, this.near, this.far);
        GL.uniformMatrix4fv(WEBGL_APP.shader.propertyLocationProjectionMatrix, false, this.projectionMatrix);
    };
    Camera.prototype.updateViewMatrix = function () {
        this.moveCamera();
        this.rotateCamera();
        vec3.add(this.target, this.position, this.lookDirection);
        mat4.lookAt(this.viewMatrix, this.position, this.target, vec3.fromValues(0, 1, 0));
        GL.uniformMatrix4fv(WEBGL_APP.shader.propertyLocationViewMatrix, false, this.viewMatrix);
    };
    Camera.prototype.moveCamera = function () {
        var forward = vec3.create();
        vec3.normalize(forward, this.lookDirection);
        var right = vec3.create();
        vec3.cross(right, forward, vec3.fromValues(0, 1, 0));
        var up = vec3.create();
        vec3.cross(up, forward, right);
        var input = WEBGL_APP.input;
        var movementDirection = vec3.create();
        if (input.pressed.w) {
            vec3.scale(movementDirection, forward, this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.s) {
            vec3.scale(movementDirection, forward, -this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.a) {
            vec3.scale(movementDirection, right, -this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.d) {
            vec3.scale(movementDirection, right, this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        // up/down
        if (input.pressed.q) {
            vec3.scale(movementDirection, up, this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.e) {
            vec3.scale(movementDirection, up, -this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
    };
    Camera.prototype.rotateCamera = function () {
        if (!WEBGL_APP.isLocked) {
            return;
        }
        var mouseDelta = WEBGL_APP.input.mouseDelta;
        this.yaw += mouseDelta[0] * this.rotateSpeed;
        this.pitch -= mouseDelta[1] * this.rotateSpeed;
        this.pitch = WEBGL_APP.mathUtils.clamp(this.pitch, -1.5, 1.5);
        this.lookDirection[0] = Math.cos(this.pitch) * Math.cos(this.yaw);
        this.lookDirection[1] = Math.sin(this.pitch);
        this.lookDirection[2] = Math.cos(this.pitch) * Math.sin(this.yaw);
    };
    return Camera;
}());
