"use strict";
var MathUtils = /** @class */ (function () {
    function MathUtils() {
    }
    MathUtils.prototype.clamp = function (value, min, max) {
        if (value < min) {
            return min;
        }
        if (value > max) {
            return max;
        }
        return value;
    };
    return MathUtils;
}());
