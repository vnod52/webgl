"use strict";
var WebGLApp = /** @class */ (function () {
    function WebGLApp() {
        this.input = new Input();
        this.mathUtils = new MathUtils();
        this.isLocked = false;
    }
    WebGLApp.prototype.initialize = function () {
        var _this = this;
        this.lockCursor();
        this.setWebGLPreferences();
        this.shader = new Shader();
        this.mesh = new Mesh();
        this.camera = new Camera();
        this.ambientLight = new AmbientLight();
        this.sceneObjects = [
            new SceneObject({ x: 0, y: 0, z: 1 }, { x: 45, y: 45, z: 0 }, { x: 0.5, y: 0.5, z: 0.5 }),
            new SceneObject({ x: 0.75, y: 0, z: 1 }, { x: 0, y: 0, z: 90 }),
            new SceneObject({ x: -0.75, y: 0, z: 1 })
        ];
        this.textureManager = new TextureManager();
        requestAnimationFrame(function () { _this.update(); });
    };
    WebGLApp.prototype.setWebGLPreferences = function () {
        GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);
        GL.clearColor(0, 0, 0, 1);
        GL.enable(GL.DEPTH_TEST);
    };
    WebGLApp.prototype.update = function () {
        var _this = this;
        if (!this.textureManager.isReady()) {
            requestAnimationFrame(function () { _this.update(); });
            return;
        }
        this.draw();
        this.sceneObjects.forEach(function (e) {
            e.update();
        });
        this.camera.update();
        this.input.reset();
        requestAnimationFrame(function () { _this.update(); });
    };
    WebGLApp.prototype.draw = function () {
        GL.clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT);
    };
    WebGLApp.prototype.lockCursor = function () {
        var _this = this;
        CANVAS.onclick = function () {
            CANVAS.requestPointerLock();
        };
        CANVAS.requestPointerLock();
        document.addEventListener('pointerlockchange', function () {
            _this.isLocked = (document.pointerLockElement == CANVAS);
        }, false);
    };
    return WebGLApp;
}());
