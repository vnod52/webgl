"use strict";
var FLOAT_BYTE_SIZE = 4;
var STRIDE = 9 * FLOAT_BYTE_SIZE;
var VERTEX_COLOR_OFFSET = 3 * FLOAT_BYTE_SIZE;
var VERTEX_TEX_COORD_OFFSET = 7 * FLOAT_BYTE_SIZE;
var VERTEX_SOURCE = "#version 300 es\n\n    in vec4 position;\n    in vec4 color;\n    in float pointSize;\n    in vec2 texCoord;\n\n    uniform mat4 projectionMatrix;\n    uniform mat4 viewMatrix;\n    uniform mat4 modelMatrix;\n\n    out vec4 linkedColor;\n    out vec2 linkedTexCoord;\n    \n    void main() {\n        gl_Position = projectionMatrix * viewMatrix * modelMatrix * position;\n        gl_PointSize = pointSize;\n        linkedColor = color;\n        linkedTexCoord = texCoord;\n    }";
var FRAGMENT_SOURCE = "#version 300 es\n    precision mediump float;\n\n    in vec4 linkedColor;\n    in vec2 linkedTexCoord;\n\n    uniform sampler2D texture0;\n    uniform sampler2D texture1;\n    uniform vec3 ambientColor;\n    uniform float ambientIntensity;\n\n    out vec4 outColor;\n\n    void main() {\n        outColor = mix(texture(texture0, linkedTexCoord), texture(texture1, linkedTexCoord), 0.5) * linkedColor * vec4(ambientColor * ambientIntensity, 1.0);\n        // outColor = linkedColor * vec4(ambientColor * ambientIntensity, 1.0);\n    }";
var Shader = /** @class */ (function () {
    function Shader() {
        this.propertyLocationTextures = [];
        this.program = this.getNewShaderProgram();
        this.getPropertyLocations();
    }
    Shader.prototype.getNewShaderProgram = function () {
        var vertexShader = this.getAndCompileShader(VERTEX_SOURCE, GL.VERTEX_SHADER);
        var fragmentShader = this.getAndCompileShader(FRAGMENT_SOURCE, GL.FRAGMENT_SHADER);
        var newShaderProgram = GL.createProgram();
        GL.attachShader(newShaderProgram, vertexShader);
        GL.attachShader(newShaderProgram, fragmentShader);
        GL.linkProgram(newShaderProgram);
        GL.useProgram(newShaderProgram);
        return newShaderProgram;
    };
    Shader.prototype.getAndCompileShader = function (shaderSource, shaderType) {
        var newShader = GL.createShader(shaderType);
        GL.shaderSource(newShader, shaderSource);
        GL.compileShader(newShader);
        console.log(shaderSource);
        if (!GL.getShaderParameter(newShader, GL.COMPILE_STATUS)) {
            alert(GL.getShaderInfoLog(newShader));
        }
        return newShader;
    };
    Shader.prototype.getPropertyLocations = function () {
        this.propertyLocationPosition = GL.getAttribLocation(this.program, "position");
        this.propertyLocationColor = GL.getAttribLocation(this.program, "color");
        this.propertyLocationPointSize = GL.getAttribLocation(this.program, "pointSize");
        this.propertyLocationTexCoord = GL.getAttribLocation(this.program, "texCoord");
        this.propertyLocationAmbientColor = GL.getUniformLocation(this.program, "ambientColor");
        this.propertyLocationAmbientIntensity = GL.getUniformLocation(this.program, "ambientIntensity");
        this.propertyLocationModelMatrix = GL.getUniformLocation(this.program, "modelMatrix");
        this.propertyLocationProjectionMatrix = GL.getUniformLocation(this.program, "projectionMatrix");
        this.propertyLocationViewMatrix = GL.getUniformLocation(this.program, "viewMatrix");
        this.propertyLocationTextures = [
            GL.getUniformLocation(this.program, "texture0"),
            GL.getUniformLocation(this.program, "texture1")
        ];
        GL.enableVertexAttribArray(this.propertyLocationPosition);
        GL.enableVertexAttribArray(this.propertyLocationColor);
        GL.enableVertexAttribArray(this.propertyLocationTexCoord);
    };
    return Shader;
}());
