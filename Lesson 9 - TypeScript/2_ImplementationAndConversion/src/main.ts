declare var glMatrix: any;
const { mat4, vec2, vec3 } = glMatrix;

const CANVAS: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("render-canvas");
// const GL = CANVAS.getContext("webgl2");
const GL: WebGL2RenderingContext = <WebGL2RenderingContext>CANVAS.getContext("webgl2", {preserveDrawingBuffer: true});
const WEBGL_APP: WebGLApp  = new WebGLApp();

WEBGL_APP.initialize();