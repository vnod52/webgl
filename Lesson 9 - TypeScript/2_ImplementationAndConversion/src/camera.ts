class Camera {
    constructor() {
        this.update();
    }

    viewMatrix: any = mat4.create();
    projectionMatrix: any = mat4.create();
    near: number = 1;
    far: number = 200;
    fovRadians: number = 60 * Math.PI/180;
    // We are working here.
    moveSpeed: number = 0.2;
    rotateSpeed: number = 0.005;
    pitch: number = 0;
    yaw: number = -1 * Math.PI/2;
    target: any = vec3.create();
    position: any = vec3.create();
    lookDirection: any = vec3.fromValues(0, 0, -1);

    update() {
        if (CANVAS.width != CANVAS.clientWidth || CANVAS.height != CANVAS.clientHeight) {
            this.updateViewport();
        }

        this.updateProjectionMatrix();
        this.updateViewMatrix();
    }

    updateViewport() {
        CANVAS.width = CANVAS.clientWidth;
        CANVAS.height = CANVAS.clientHeight;
        GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);
    }

    updateProjectionMatrix() {
        var aspect: number = CANVAS.clientWidth / CANVAS.clientHeight;

        // var projectionMatrix = this.perspective(aspect);
        mat4.perspective(
            this.projectionMatrix
        ,   this.fovRadians
        ,   aspect
        ,   this.near
        ,   this.far
        );

        GL.uniformMatrix4fv(WEBGL_APP.shader.propertyLocationProjectionMatrix, false, this.projectionMatrix);
    }

    updateViewMatrix() {
        this.moveCamera();
        this.rotateCamera();

        vec3.add(this.target, this.position, this.lookDirection);
        mat4.lookAt(this.viewMatrix, this.position, this.target, vec3.fromValues(0, 1, 0));

        GL.uniformMatrix4fv(WEBGL_APP.shader.propertyLocationViewMatrix, false, this.viewMatrix);
    }

    moveCamera() {
        var forward: any = vec3.create();
        vec3.normalize(forward, this.lookDirection);
        var right: any = vec3.create();
        vec3.cross(right, forward, vec3.fromValues(0, 1, 0));
        var up: any = vec3.create();
        vec3.cross(up, forward, right);

        var input: any = WEBGL_APP.input;
        var movementDirection: any = vec3.create();

        if (input.pressed.w) {
            vec3.scale(movementDirection, forward, this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.s) {
            vec3.scale(movementDirection, forward, -this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.a) {
            vec3.scale(movementDirection, right, -this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.d) {
            vec3.scale(movementDirection, right, this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        // up/down
        if (input.pressed.q) {
            vec3.scale(movementDirection, up, this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
        if (input.pressed.e) {
            vec3.scale(movementDirection, up, -this.moveSpeed);
            vec3.add(this.position, this.position, movementDirection);
        }
    }

    rotateCamera() {
        if (!WEBGL_APP.isLocked) { return; }

        var mouseDelta: any = WEBGL_APP.input.mouseDelta;

        this.yaw += mouseDelta[0] * this.rotateSpeed;
        this.pitch -= mouseDelta[1] * this.rotateSpeed;
        this.pitch = WEBGL_APP.mathUtils.clamp(this.pitch, -1.5, 1.5);

        this.lookDirection[0] = Math.cos(this.pitch) * Math.cos(this.yaw);
        this.lookDirection[1] = Math.sin(this.pitch);
        this.lookDirection[2] = Math.cos(this.pitch) * Math.sin(this.yaw);
    }
}