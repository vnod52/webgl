class TextureManager {
    constructor() {
        this.textureBuffers = [
            this.createTexture("./images/Clouds.png"),
            this.createTexture("./images/Beagle.jpg")
        ];
    }

    textureBuffers: WebGLTexture[] = [];
    readyCount = 0;

    createTexture(_url: string): WebGLTexture {
        var textureBuffer: WebGLTexture = <WebGLTexture>GL.createTexture();
        var image: any = new Image();
        image.src = _url;
        image.onload = () => {
            GL.bindTexture(GL.TEXTURE_2D, textureBuffer);
            GL.pixelStorei(GL.UNPACK_FLIP_Y_WEBGL, true);
            GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGB, GL.RGB, GL.UNSIGNED_BYTE, image);
        
            GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);
            GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST);
        
            this.setIsReady();
        }

        return textureBuffer;
    }

    isReady() {
        return this.textureBuffers.length == this.readyCount;
    }

    setIsReady() {
        if (this.textureBuffers.length == ++this.readyCount) {
            this.setShaderProperties();
        }
    }

    setShaderProperties() {
        var shader = WEBGL_APP.shader;
        GL.useProgram(shader.program);

        for (let i = 0; i < this.textureBuffers.length; i++) {
            GL.activeTexture(GL.TEXTURE0 + i);
            GL.bindTexture(GL.TEXTURE_2D, this.textureBuffers[i]);
            GL.uniform1i(shader.propertyLocationTextures[i], i);
        }
    }
}