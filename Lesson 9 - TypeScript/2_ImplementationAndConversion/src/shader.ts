const FLOAT_BYTE_SIZE: number = 4;
const STRIDE: number = 9 * FLOAT_BYTE_SIZE;
const VERTEX_COLOR_OFFSET: number = 3 * FLOAT_BYTE_SIZE;
const VERTEX_TEX_COORD_OFFSET: number = 7 * FLOAT_BYTE_SIZE;
const VERTEX_SOURCE: string = 
    `#version 300 es

    in vec4 position;
    in vec4 color;
    in float pointSize;
    in vec2 texCoord;

    uniform mat4 projectionMatrix;
    uniform mat4 viewMatrix;
    uniform mat4 modelMatrix;

    out vec4 linkedColor;
    out vec2 linkedTexCoord;
    
    void main() {
        gl_Position = projectionMatrix * viewMatrix * modelMatrix * position;
        gl_PointSize = pointSize;
        linkedColor = color;
        linkedTexCoord = texCoord;
    }`
;
const FRAGMENT_SOURCE: string = 
    `#version 300 es
    precision mediump float;

    in vec4 linkedColor;
    in vec2 linkedTexCoord;

    uniform sampler2D texture0;
    uniform sampler2D texture1;
    uniform vec3 ambientColor;
    uniform float ambientIntensity;

    out vec4 outColor;

    void main() {
        outColor = mix(texture(texture0, linkedTexCoord), texture(texture1, linkedTexCoord), 0.5) * linkedColor * vec4(ambientColor * ambientIntensity, 1.0);
        // outColor = linkedColor * vec4(ambientColor * ambientIntensity, 1.0);
    }`
;

class Shader {
    constructor() {
        this.program = <WebGLProgram>this.getNewShaderProgram();
        this.getPropertyLocations();
    }

    program: WebGLProgram;
    propertyLocationPosition: number;
    propertyLocationPointSize: number;
    propertyLocationColor: number;
    propertyLocationTexCoord: number;
    propertyLocationAmbientColor: WebGLUniformLocation;
    propertyLocationAmbientIntensity: WebGLUniformLocation;
    propertyLocationProjectionMatrix: WebGLUniformLocation;
    propertyLocationViewMatrix: WebGLUniformLocation;
    propertyLocationModelMatrix: WebGLUniformLocation;
    propertyLocationTextures: WebGLUniformLocation[] = [];

    getNewShaderProgram(): WebGLProgram {
        var vertexShader: WebGLShader = <WebGLShader>this.getAndCompileShader(VERTEX_SOURCE, GL.VERTEX_SHADER);
        var fragmentShader: WebGLShader = <WebGLShader>this.getAndCompileShader(FRAGMENT_SOURCE, GL.FRAGMENT_SHADER);
    
        var newShaderProgram: WebGLProgram = <WebGLProgram>GL.createProgram();
        GL.attachShader(newShaderProgram, vertexShader);
        GL.attachShader(newShaderProgram, fragmentShader);
        GL.linkProgram(newShaderProgram);
        GL.useProgram(newShaderProgram);
    
        return newShaderProgram;
    }

    getAndCompileShader(shaderSource: string, shaderType: number): WebGLShader {
        var newShader: WebGLShader = <WebGLShader>GL.createShader(shaderType);
        GL.shaderSource(newShader, shaderSource);
        GL.compileShader(newShader);
    
        console.log(shaderSource);
    
        if (!GL.getShaderParameter(newShader, GL.COMPILE_STATUS)) {
            alert(GL.getShaderInfoLog(newShader));
        }
    
        return newShader;
    }
    
    getPropertyLocations() {
        this.propertyLocationPosition  = GL.getAttribLocation(this.program, "position");
        this.propertyLocationColor     = GL.getAttribLocation(this.program, "color");
        this.propertyLocationPointSize = GL.getAttribLocation(this.program, "pointSize");
        this.propertyLocationTexCoord  = GL.getAttribLocation(this.program, "texCoord");
        
        this.propertyLocationAmbientColor     = <WebGLUniformLocation>GL.getUniformLocation(this.program, "ambientColor");
        this.propertyLocationAmbientIntensity = <WebGLUniformLocation>GL.getUniformLocation(this.program, "ambientIntensity");
        this.propertyLocationModelMatrix      = <WebGLUniformLocation>GL.getUniformLocation(this.program, "modelMatrix");
        this.propertyLocationProjectionMatrix = <WebGLUniformLocation>GL.getUniformLocation(this.program, "projectionMatrix");
        this.propertyLocationViewMatrix       = <WebGLUniformLocation>GL.getUniformLocation(this.program, "viewMatrix");
        this.propertyLocationTextures = [
            <WebGLUniformLocation>GL.getUniformLocation(this.program, "texture0"),
            <WebGLUniformLocation>GL.getUniformLocation(this.program, "texture1")
        ] 
    
        GL.enableVertexAttribArray(this.propertyLocationPosition);
        GL.enableVertexAttribArray(this.propertyLocationColor);
        GL.enableVertexAttribArray(this.propertyLocationTexCoord);
    }
}