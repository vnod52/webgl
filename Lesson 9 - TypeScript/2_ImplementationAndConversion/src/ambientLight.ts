class AmbientLight {
    constructor() {
        this.updateAmbientLight();
    }

    color: any = vec3.fromValues(0.4, 0.4, 1);
    intensity: number = 1;

    updateAmbientLight() {
        GL.uniform3fv(WEBGL_APP.shader.propertyLocationAmbientColor, this.color);
        GL.uniform1f(WEBGL_APP.shader.propertyLocationAmbientIntensity, this.intensity);
    }
}