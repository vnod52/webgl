import { mat4 } from "gl-matrix";
import { MAIN } from "./main";


export class Transform {
    constructor(_position: Object, _rotation: Object, _scale: Object) {
        this.position = _position;
        this.rotation = _rotation;
        this.scale = _scale;
        this.modelMatrix = mat4.create();
    }

    position: any = { x:0, y:0, z:0 };
    rotation: any = { x:0, y:0, z:0 };
    scale: any    = { x:0, y:0, z:0 };
    modelMatrix: any;

    applyTransforms() {
        mat4.identity(this.modelMatrix);
        mat4.translate(this.modelMatrix, this.modelMatrix, [this.position.x, this.position.y, this.position.z]);
        mat4.rotateZ(this.modelMatrix, this.modelMatrix, this.rotation.z);
        mat4.rotateY(this.modelMatrix, this.modelMatrix, this.rotation.y);
        mat4.rotateX(this.modelMatrix, this.modelMatrix, this.rotation.x);
        mat4.scale(this.modelMatrix, this.modelMatrix, [this.scale.x, this.scale.y, this.scale.z]);

        MAIN.GL.uniformMatrix4fv(MAIN.WEBGL_APP.shader.propertyLocationModelMatrix, false, this.modelMatrix);
    }
}