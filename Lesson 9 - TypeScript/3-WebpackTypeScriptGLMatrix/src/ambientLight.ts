import { vec3 } from "gl-matrix";
import { MAIN } from "./main";

export class AmbientLight {
    constructor() {
        this.updateAmbientLight();
    }

    color: any = vec3.fromValues(0.4, 0.4, 1);
    intensity: number = 1;

    updateAmbientLight() {
        MAIN.GL.uniform3fv(MAIN.WEBGL_APP.shader.propertyLocationAmbientColor, this.color);
        MAIN.GL.uniform1f(MAIN.WEBGL_APP.shader.propertyLocationAmbientIntensity, this.intensity);
    }
}