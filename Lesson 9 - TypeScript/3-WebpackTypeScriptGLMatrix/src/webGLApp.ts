import { SceneObject } from "./sceneObject";
import { Camera } from "./camera";
import { Shader } from "./shader";
import { TextureManager } from "./textureManager";
import { Mesh } from "./mesh";
import { MAIN } from "./main";
import { AmbientLight } from "./ambientLight";
import { Input } from "./input";
import { MathUtils } from "./utilities/mathUtils";

export class WebGLApp {
    constructor() {
    }

    mesh: Mesh;
    sceneObjects: SceneObject[];
    textureManager: TextureManager;
    shader: Shader;
    camera: Camera;
    ambientLight: AmbientLight;
    input = new Input();
    mathUtils: MathUtils = new MathUtils();
    isLocked: boolean = false;

    initialize() {
        this.lockCursor();
        this.setWebGLPreferences();

        this.shader = new Shader();
        this.mesh = new Mesh();
        this.camera = new Camera();
        this.ambientLight = new AmbientLight();

        this.sceneObjects = [
            new SceneObject({x:0, y:0, z:1}, {x:45, y:45, z:0}, {x:0.5,y:0.5,z:0.5})
        ,   new SceneObject({x:0.75, y:0, z:1}, {x:0, y:0, z:90})
        ,   new SceneObject({x:-0.75, y:0, z:1})
        ];
        this.textureManager = new TextureManager();

        requestAnimationFrame(() => { this.update() });
    }

    setWebGLPreferences() {
        MAIN.GL.viewport(0, 0, MAIN.CANVAS.clientWidth, MAIN.CANVAS.clientHeight);
        MAIN.GL.clearColor(0, 0, 0, 1);
        MAIN.GL.enable(MAIN.GL.DEPTH_TEST);
    }

    update() {
        if (!this.textureManager.isReady()) {
            requestAnimationFrame(() => { this.update() });
            return;
        }

        this.draw();

        this.sceneObjects.forEach(e => {
            e.update();
        });

        this.camera.update();

        this.input.reset();

        requestAnimationFrame(() => { this.update() });
    }

    draw() {
        MAIN.GL.clear(MAIN.GL.COLOR_BUFFER_BIT | MAIN.GL.DEPTH_BUFFER_BIT);
    }

    lockCursor() {
        MAIN.CANVAS.onclick = () => {
            MAIN.CANVAS.requestPointerLock();
        }

        MAIN.CANVAS.requestPointerLock();

        document.addEventListener('pointerlockchange', () => {
            this.isLocked = (document.pointerLockElement == MAIN.CANVAS);
        }, false);
    }
}