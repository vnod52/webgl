const CANVAS = document.getElementById("render-canvas");
const GL = CANVAS.getContext("webgl2");
const vertexShaderSource = 
    `#version 300 es

    in vec4 position;
    in vec4 color;
    in float pointSize;

    out vec4 linkedColor;


    void main(){
        gl_Position = position;
        gl_PointSize = pointSize;
        linkedColor = color;
    }`
;

const fragmentShaderSource = 
    `#version 300 es
    precision mediump float;

    in vec4 linkedColor

    out vec4 outColor;

    void main(){
        outColor = linkedColor;
    }`
;

var propertyLocationPosition;
var propertyLocationPointSize;
var propertyLocationColor;

var program;

start();

function start(){
    //Create the size of the GL canvas
    GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);

    //Change the background color of the canvas
    GL.clearColor(0, 0, 0, 1);

    program = createShader();
    getPropertyLocation();
    setShaderProperties();
    
    draw();
    
}

function draw(){
    GL.clear(GL.COLOR_BUFFER_BIT);
    GL.drawArrays(GL.POINTS, 0, 1);
}

function createShader() {
    var vertexShader = getAndCompileShader(vertexShaderSource, GL.VERTEX_SHADER);
    var fragmentShader = getAndCompileShader(fragmentShaderSource,GL.FRAGMENT_SHADER);

    var newShaderProgram = GL.createProgram();
    GL.attachShader(newShaderProgram, vertexShader);
    GL.attachShader(newShaderProgram, fragmentShader);
    GL.linkProgram(newShaderProgram);
    GL.useProgram(program);

    return newShaderProgram
}

function getAndCompileShader(shaderSource, shaderType) {
    var newShader = GL.createShader(shaderType);
    GL.shaderSource(newShader, shaderSource);
    GL.compileShader(newShader);

    console.log(shaderSource);

    if (!GL.getShaderParameter(newShader, GL.COMPILE_STATUS)){
        alert(GL.getShaderInfoLog(newShader));
        return null;
    }

    return newShader
}

function getPropertyLocation(){
    propertyLocationPosition = GL.getAttribLocation(program, "position");
    propertyLocationPointSize = GL.getAttribLocation(program, "pointSize");
    propertyLocationColor = GL.getAttribLocation(program, "color");

}

function setShaderProperties() {
    GL.useProgram(program);

    GL.vertexAttrib3f(propertyLocationPosition, 0.8, 0.10, 0.2);
    GL.vertexAttrib4f(propertyLocationColor, 0.8, 0.2, 0.2, 1);
    GL.vertexAttrib1f(propertyLocationPointSize, 20);
}