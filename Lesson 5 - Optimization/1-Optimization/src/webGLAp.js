class WebGLApp {
    constructor() {

    }

    triangle;
    textureManager;
    shader;

    initialize() {
        this.setWebGLPreferences();

        this.shader = new shader();
        this.triangle = new triangle();
        this.textureManager = new textureManager();

        requestAnimationFrame(
            () => {this.update}
        );

    }

    setWebGLPreferences() {
        //Create the size of the GL canvas
        GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);

        //Change the background color of the canvas
        GL.clearColor(0, 0, 0, 1);

    }

    update() {
        if (!textureManager.isReady()) {
            requestAnimationFrame(update);

            return;
        }

        this.draw()

        triangle.update();

        requestAnimationFrame(update);

    }

    draw() {
        GL.clear(GL.COLOR_BUFFER_BIT);

    }




}