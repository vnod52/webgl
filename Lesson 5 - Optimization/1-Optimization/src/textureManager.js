class TextureManager {
    constructor() {
        this.textureBuffers = [
            // this.createTexture("./images/texture.png")

            
        ];
    }

    textureBuffers = [];

    createTexture(_url) {
        var textureBuffer = GL.createTexture();
        textureBuffer.image = new Image();

        textureBuffer.image.onload = function() {
            GL.bindTexture(GL.TEXTURE_2D, textureBuffer);
            GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGB, GL.RGB, GL.UNSIGNED_BYTE, textureBuffer.image);

            GL.textParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);
            GL.textParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST);

            console.log(textureBuffer.image.complete);

            start();
        }

        return textureBuffer;

    }

    isReady() {
        return true;

        this.textureBuffers.forEach(e => {
            if(!e.image.complete) {
                console.log(e.image, e.image.complete);
                return false;
            }
        });

        }
    

    setShaderProperties() {
        var shader = WEBGL_APP.shader;
        GL.useProgram(shader.program);

        for (let i = 0; i < this.textureBuffers.length; i++) {
            GL.activeTexture(GL.TEXTURE0 + i);
            GL.bindTexture(GL.TEXTURE_2D, textureBuffers[i]);
            GL.uniform1i(shader.propertyLocationMainTextures[i], i);
        }


    }

}