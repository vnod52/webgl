const FLOAT_BYTE_SIZE = 4;
const STRIDE = 9 * FLOAT_BYTE_SIZE;
const VERTEX_COLOR_OFFSET = 3 * FLOAT_BYTE_SIZE;
const VERTEX_TEX_COORD_OFFSET = 7 * FLOAT_BYTE_SIZE;

    VERTEX_SOURCE =
        `#version 300 es

        in vec4 position;
        in vec4 color;
        in float pointSize;
        in vec2 texCoord;

        out vec4 linkedColor;
        out vec2 linkedTexCoord;

        void main(){
            gl_Position = position;
            gl_PointSize = pointSize;
            linkedColor = color;
            linkedTexCoord = texCoord;
    }`,

    FRAGMENT_SOURCE =
        `#version 300 es
        precision mediump float;

        in vec4 linkedColor;
        in vec2 linkedTexCoord;

        uniform sampler2D texture0;

        out vec4 outColor;

        void main(){
            // outColor = texture(texture0, linkedTexCoord) * linkedColor;
            outColor = linkedColor;
    }`      
;


class Shader {
    constructor() {
        this.program = this.getNewShaderProgram;
        this.getPropertyLocation();

    }

    program;
    propertyLocationPosition;
    propertyLocationPointSize;
    propertyLocationColor;
    propertyLocationTexCoord;
    propertyLocationTextures = [];

    getNewShaderProgram() {
        var vertexShader = getAndCompileShader(VERTEX_SOURCE, GL.VERTEX_SHADER);
        var fragmentShader = getAndCompileShader(FRAGMENT_SOURCE,GL.FRAGMENT_SHADER);
    
        var newShaderProgram = GL.createProgram();
        GL.attachShader(newShaderProgram, vertexShader);
        GL.attachShader(newShaderProgram, fragmentShader);
        GL.linkProgram(newShaderProgram);
        GL.useProgram(this.program);
    
        return newShaderProgram
    }

    getAndCompileShader(shaderSource, shaderType) {
        var newShader = GL.createShader(shaderType);
        GL.shaderSource(newShader, shaderSource);
        GL.compileShader(newShader);
    
        console.log(shaderSource);
    
        if (!GL.getShaderParameter(newShader, GL.COMPILE_STATUS)){
            alert(GL.getShaderInfoLog(newShader));
            return null;
        }
    
        return newShader
    }
    
    getPropertyLocation(){
        this.propertyLocationPosition    = GL.getAttribLocation(this.program, "position");
        this.propertyLocationPointSize   = GL.getAttribLocation(this.program, "pointSize");
        this.propertyLocationColor       = GL.getAttribLocation(this.program, "color");
        this.propertyLocationtexCoord    = GL.getAttribLocation(this.program, "texCoord");
        this.propertyLocationTexture = [
            GL.getUniformLocation(this.program, "texture0")
        ]
    
        GL.enableVertexAttribArray(this.propertyLocationPosition);
        GL.enableVertexAttribArray(this.propertyLocationColor);
        GL.enableVertexAttribArray(this.propertyLocationTexCoord);
    
    }
}
