class WebGLApp {
    constructor() {
    }

    triangle;
    textureManager;
    shader;

    initialize() {
        this.setWebGLPreferences();

        this.shader = new Shader();
        this.triangle = new Triangle();
        this.textureManager = new TextureManager();

        requestAnimationFrame(() => { this.update() });
    }

    setWebGLPreferences() {
        GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);
        GL.clearColor(0, 0, 0, 1);
    }

    update() {
        if (!this.textureManager.isReady()) {
            requestAnimationFrame(() => { this.update() });
            return;
        }

        this.draw();

        this.triangle.update();

        requestAnimationFrame(() => { this.update() });
    }

    draw() {
        GL.clear(GL.COLOR_BUFFER_BIT);
    }
}