class Triangle {
    constructor() {
        this.createVertices();
        this.setShaderProperties();

        this.angle = 0;
    }

    vertexDataBuffer;
    vertices;
    angle;

    createVertices() {
        this.vertices = [
            -0.5, -0.5, 0.0,    1.0, 0.0, 0.0, 1.0,     0.0, 0.0,
             0.5, -0.5, 0.0,    0.0, 1.0, 0.0, 1.0,     1.0, 0.0,
             0.0,  0.5, 0.0,    0.0, 0.0, 1.0, 1.0,     0.5, 1.0
        ];
    
        this.vertexDataBuffer = GL.createBuffer();
        GL.bindBuffer(GL.ARRAY_BUFFER, this.vertexDataBuffer);
        GL.bufferData(GL.ARRAY_BUFFER, new Float32Array(this.vertices), GL.STATIC_DRAW);
    }

    update() {
        this.transform();

        this.draw();
    }

    draw() {
        GL.drawArrays(GL.TRIANGLES, 0, 3);
    }

    setShaderProperties() {
        var shader = WEBGL_APP.shader;
        GL.useProgram(shader.program);

        GL.vertexAttribPointer(shader.propertyLocationPosition, 3, GL.FLOAT, false, STRIDE, 0);
        GL.vertexAttribPointer(shader.propertyLocationColor, 4, GL.FLOAT, false, STRIDE, VERTEX_COLOR_OFFSET);
        GL.vertexAttribPointer(shader.propertyLocationTexCoord, 2, GL.FLOAT, false, STRIDE, VERTEX_TEX_COORD_OFFSET);

        GL.vertexAttrib1f(shader.propertyLocationPointSize, 50);
    }

    transform() {
        this.angle += 1;
        var radians = (Math.PI * 2) * (this.angle/360);
        var cos = Math.cos(radians);
        var sin = Math.sin(radians);

        var identityMatrix = [
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1,
        ];

        var matrixRotateZ = [
            cos, -sin, 0, 0,
            sin,  cos, 0, 0,
            0,    0,   1, 0,
            0,    0,   0, 1,
        ];

        var tx = 0.5, ty = 0.5, tz = 0;
        var matrixTranslate = [
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            tx, ty, tz, 1,
        ];

        var sx = 0.5, sy = 0.5, sz = 0;
        var matrixScale = [
            sx + cos, 0, 0, 0,
            0, sy + sin, 0, 0,
            0, 0, sz, 0,
            0, 0, 0, 1,
        ];

        var modelMatrixLocation = GL.getUniformLocation(WEBGL_APP.shader.program, "modelMatrix");
        GL.uniformMatrix4fv(modelMatrixLocation, false, matrixScale);
    }
}