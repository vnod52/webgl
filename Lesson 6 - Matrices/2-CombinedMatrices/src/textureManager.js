class TextureManager {
    constructor() {
        this.textureBuffers = [
            this.createTexture("./images/Clouds.png"),
            this.createTexture("./images/Beagle.jpg")
        ];
    }

    textureBuffers = [];

    createTexture(_url) {
        var textureBuffer = GL.createTexture();
        textureBuffer.image = new Image();
        textureBuffer.image.src = _url;

        textureBuffer.image.onload = function() {
            GL.bindTexture(GL.TEXTURE_2D, textureBuffer);
            GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGB, GL.RGB, GL.UNSIGNED_BYTE, textureBuffer.image);
        
            GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);
            GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST);
        
            console.log(textureBuffer.image.src, textureBuffer.image.complete);
        }

        return textureBuffer;
    }

    isReady() {
        this.textureBuffers.forEach(e => {
            if(!e.image.complete) {
                console.log(e.image, e.image.complete);
                return false;
            }
        });

        this.setShaderProperties();
        return true;
    }

    setShaderProperties() {
        var shader = WEBGL_APP.shader;
        GL.useProgram(shader.program);

        for (let i = 0; i < this.textureBuffers.length; i++) {
            GL.activeTexture(GL.TEXTURE0 + i);
            GL.bindTexture(GL.TEXTURE_2D, this.textureBuffers[i]);
            GL.uniform1i(shader.propertyLocationTextures[i], i);
        }
    }
}