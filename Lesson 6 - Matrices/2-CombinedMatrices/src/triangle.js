class Triangle extends Transform {
    constructor() {

        super();
        this.createVertices();
        this.setShaderProperties();

        this.angle = 0;
    }

    vertexDataBuffer;
    vertices;
    angle;

    createVertices() {
        this.vertices = [
            -0.5, -0.5, 0.0,    1.0, 0.0, 0.0, 1.0,     0.0, 0.0,
             0.5, -0.5, 0.0,    0.0, 1.0, 0.0, 1.0,     1.0, 0.0,
             0.0,  0.5, 0.0,    0.0, 0.0, 1.0, 1.0,     0.5, 1.0
        ];
    
        this.vertexDataBuffer = GL.createBuffer();
        GL.bindBuffer(GL.ARRAY_BUFFER, this.vertexDataBuffer);
        GL.bufferData(GL.ARRAY_BUFFER, new Float32Array(this.vertices), GL.STATIC_DRAW);
    }

    update() {
        this.transform();
        this.applyTransform();

        this.draw();
    }

    draw() {
        GL.drawArrays(GL.TRIANGLES, 0, 3);
    }

    setShaderProperties() {
        var shader = WEBGL_APP.shader;
        GL.useProgram(shader.program);

        GL.vertexAttribPointer(shader.propertyLocationPosition, 3, GL.FLOAT, false, STRIDE, 0);
        GL.vertexAttribPointer(shader.propertyLocationColor, 4, GL.FLOAT, false, STRIDE, VERTEX_COLOR_OFFSET);
        GL.vertexAttribPointer(shader.propertyLocationTexCoord, 2, GL.FLOAT, false, STRIDE, VERTEX_TEX_COORD_OFFSET);

        GL.vertexAttrib1f(shader.propertyLocationPointSize, 50);
    }

    transform() {
        this.angle += 0.1;

        this.position = {
            x: 0.5,
            y: 0.5,
            z: 0,
        };

        this.rotation = {
            x: this.angle,
            y: this.angle * 2,
            z: 0,
        };

        this.scale = {
            x: 1,
            y: 1,
            z: 0,
        };
   
    }
}