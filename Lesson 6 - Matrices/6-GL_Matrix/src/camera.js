class Camera {
    constructor() {
        this.update();
    }

    projectionMatrix = mat4.create();
    near = 1;
    far = 200;
    fovRadians = 60 * Math.PI/180;

    update() {
        if (CANVAS.width != CANVAS.clientWidth || CANVAS.height != CANVAS.clientHeight){
            this.updateViewport();
        }
        this.updateProjectionMatrix();

    }

    updateViewport() {
        CANVAS.width = CANVAS.clientWidth;
        CANVAS.height = CANVAS.clientHeight;
        GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);
    }

    updateProjectionMatrix() {
        var aspect = CANVAS.clientWidth / CANVAS.clientHeight;
        mat4.perspective(this.projectionMatrix, this.fovRadians, aspect, this.near, this.far);

        GL.uniformMatrix4fv(WEBGL_APP.shader.propertyLocationProjectionMatrix, false, this.projectionMatrix);
    }
}