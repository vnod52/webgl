class Transform {
    constructor(_position, _rotation, _scale) {
        this.position = _position;
        this.rotation = _rotation;
        this.scale = _scale
        this.modelMatrix = mat4.create();

    }

    position = { x: 0, y: 0, z: 0 };
    rotation = { x: 0, y: 0, z: 0 };
    scale = { x: 0, y: 0, z: 0 };
    modelMatrix;

    applyTransform() {
        mat4.identity(this.modelMatrix);
        mat4.translate(this.modelMatrix, this.modelMatrix, [this.position.x, this.position.y, this.position.z]);
        mat4.rotateZ(this.modelMatrix, this.modelMatrix, this.rotation.z);
        mat4.rotateY(this.modelMatrix, this.modelMatrix, this.rotation.y);
        mat4.rotateX(this.modelMatrix, this.modelMatrix, this.rotation.x);
        mat4.scale(this.modelMatrix, this.modelMatrix, [this.scale.x, this.scale.y, this.scale.z]);

        GL.uniformMatrix4fv(WEBGL_APP.shader.propertyLocationModelMatrix, false, this.modelMatrix);
    }
}
