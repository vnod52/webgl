class SceneObject extends Transform {
    constructor(_position = {x:0,y:0,z:0}, _rotation = {x:0,y:0,z:0}, _scale = {x:1,y:1,z:1}) {

        super(_position, _rotation, _scale);

        this.intialPosition = _position;
    }

    angle = Math.random() * (180 - -180) + -180;
    intialPosition;

    update() {
        this.transform();
        this.applyTransform();

        this.draw();
    }

    draw() {
        GL.bindBuffer(GL.ARRAY_BUFFER, WEBGL_APP.mesh.vertexBuffer);
        GL.drawArrays(GL.TRIANGLES, 0, 3);
    }

    transform() {
        this.angle += 0.10;

        this.position = {
            x: this.intialPosition.x + Math.sin(this.angle *0.05) * 0.5,
            y: this.intialPosition.y + Math.sin(this.angle *0.05) * 0.5,
            z: -4,
        };

        this.rotation = {
            x: this.angle,
            y: this.angle * 5,
            z: 0,
        };

        this.scale = {
            x: 1,
            y: 1,
            z: 0,
        };
   
    }
}