class WebGLApp {
    constructor() {
    }

    mesh;
    sceneObject;
    textureManager;
    shader;
    camera;

    initialize() {
        this.setWebGLPreferences();

        this.shader = new Shader();
        this.mesh = new Mesh();
        this.camera = new Camera();
        this.sceneObject = [
            new SceneObject({x:0, y:0, z:1}, {x:45, y:45, z:45}, {x:0.5, y:0.5, z:0.5}),
            new SceneObject({x:1, y:0, z:1}, {x:45, y:45, z:45}), 
            new SceneObject({x:1, y:0, z:1}) 
            ]

        this.textureManager = new TextureManager();

        requestAnimationFrame(() => { this.update() });
    }

    setWebGLPreferences() {
        GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);
        GL.clearColor(0, 0, 0, 1);
        GL.enable(GL.DEPTH_TEST);
    }


    update() {
        if (!this.textureManager.isReady()) {
            requestAnimationFrame(() => { this.update() });
            return;
        }

        this.draw();

        this.sceneObject.forEach(e => {
            e.update();
        });

        this.camera.update();

        requestAnimationFrame(() => { this.update() });
    }

    draw() {
        GL.clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT);
    }
}