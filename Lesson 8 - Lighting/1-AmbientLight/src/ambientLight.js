class AmbientLight {
    constructor() {
        this.updateAmbientLight();
    }

    color = vec3.fromValues(0.5, 0.5, 1);
    intensity = 5;

    updateAmbientLight() {
        GL.uniform3fv(WEBGL_APP.shader.propertyLocationAmbientColor, this.color);
        GL.uniform1f(WEBGL_APP.shader.propertyLocationAmbientIntensity, this.intensity);
    }
}