const CANVAS = document.getElementById("render-canvas");
const GL = CANVAS.getContext("webgl2");
const vertexShaderSource = 
`
void main(){
    gl_Position = vec4(0, 0.5, 0, 1);
    gl_PointSize = 10.0;
}
`
;

const fragmentShaderSource = 
`
void main(){
    gl_FragColor = vec4(1, 1, 0, 1);
}
`

;

start();

function start(){
    //Create the size of the GL canvas
    GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);

    //Change the background color of the canvas
    GL.clearColor(0, 0, 0, 1);

    var shaderProgram = createShader();
    GL.useProgram(shaderProgram);

    draw();
    
}

function draw(){
    GL.clear(GL.COLOR_BUFFER_BIT);
    GL.drawArrays(GL.POINTS, 0, 1);
}

function createShader() {
    var vertexShader = getAndCompileShader(vertexShaderSource, GL.VERTEX_SHADER);
    var fragmentShader = getAndCompileShader(fragmentShaderSource,GL.FRAGMENT_SHADER);

    var newShaderProgram = GL.createProgram();
    GL.attachShader(newShaderProgram, vertexShader);
    GL.attachShader(newShaderProgram, fragmentShader);

    GL.linkProgram(newShaderProgram);

    return newShaderProgram
}

function getAndCompileShader(shaderSource, shaderType) {
    var newShader = GL.createShader(shaderType);
    GL.shaderSource(newShader, shaderSource);
    GL.compileShader(newShader);

    console.log(shaderSource);

    if (!GL.getShaderParameter(newShader, GL.COMPILE_STATUS)){
        alert(GL.getShaderInfoLog(newShader));
        return null;
    }

    return newShader
}