const CANVAS = document.getElementById("render-canvas");
const GL = CANVAS.getContext("webgl2");

var shader = {
    program: null,
    propertyLocationPosition: 0,
    propertyLocationPointSize: 0,
    propertyLocationColor: 0,

    vertexSource:
    `#version 300 es

    in vec4 position;
    in vec4 color;
    in float pointSize;

    out vec4 linkedColor;

    void main(){
        gl_Position = position;
        gl_PointSize = pointSize;
        linkedColor = color;
    }`,

    fragmentSource:
    `#version 300 es
    precision mediump float;

    in vec4 linkedColor;

    out vec4 outColor;

    void main(){
        outColor = linkedColor;
    }`

};


start();

function start(){
    //Create the size of the GL canvas
    GL.viewport(0, 0, CANVAS.clientWidth, CANVAS.clientHeight);

    //Change the background color of the canvas
    GL.clearColor(0, 0, 0, 1);

    shader.program = createShader();
    getPropertyLocation();
    setShaderProperties();
    
    draw();
    
}

function draw(){
    GL.clear(GL.COLOR_BUFFER_BIT);
    GL.drawArrays(GL.POINTS, 0, 1);
}

function createShader() {
    var vertexShader = getAndCompileShader(shader.vertexSource, GL.VERTEX_SHADER);
    var fragmentShader = getAndCompileShader(shader.fragmentSource,GL.FRAGMENT_SHADER);

    var newShaderProgram = GL.createProgram();
    GL.attachShader(newShaderProgram, vertexShader);
    GL.attachShader(newShaderProgram, fragmentShader);
    GL.linkProgram(newShaderProgram);
    GL.useProgram(shader.program);

    return newShaderProgram
}

function getAndCompileShader(shaderSource, shaderType) {
    var newShader = GL.createShader(shaderType);
    GL.shaderSource(newShader, shaderSource);
    GL.compileShader(newShader);

    console.log(shaderSource);

    if (!GL.getShaderParameter(newShader, GL.COMPILE_STATUS)){
        alert(GL.getShaderInfoLog(newShader));
        return null;
    }

    return newShader
}

function getPropertyLocation(){
    shader.propertyLocationPosition = GL.getAttribLocation(shader.program, "position");
    shader.propertyLocationPointSize = GL.getAttribLocation(shader.program, "pointSize");
    shader.propertyLocationColor = GL.getAttribLocation(shader.program, "color");

}

function setShaderProperties() {
    GL.useProgram(shader.program);

    GL.vertexAttrib3f(shader.propertyLocationPosition, 0.5, 0.20, 0.3);
    GL.vertexAttrib4f(shader.propertyLocationColor, 0.0, 0.8, 0.8, 1);
    GL.vertexAttrib1f(shader.propertyLocationPointSize, 20);
}